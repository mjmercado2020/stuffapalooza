/********* --- Desktop Navigation --- *********/
theme.desktopNavigation = {};
theme.DesktopNavigationSection = (function() {
  function DesktopNavigationSection(container) {
    BT.initDesktopNavigation('#' + $(container).attr('id'), 'desktopNav');
  }
  return DesktopNavigationSection;
})();

theme.DesktopNavigationSection.prototype = _.assignIn({}, theme.DesktopNavigationSection.prototype, {
  getTopEle: function(ele) {
    return ele.hasClass('nav__item') ? ele : ele.parents('.nav__item');
  },
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    var topEle = this.getTopEle(ele);
    if(topEle.length > 0) {
      topEle.addClass('open');
    }
  },
  onBlockDeselect: function(evt) {
    var ele = $(evt.target);
    var topEle = this.getTopEle(ele);
    if(topEle.length > 0) {
      topEle.removeClass('open');
    }
  }
});

/********* --- Vertical Navigation --- *********/
theme.verticalNavigation = {};
theme.VerticalNavigationSection = (function() {
  function VerticalNavigationSection(container) {
    BT.initDesktopNavigation('#' + $(container).attr('id'), 'verticalNav');
  }

  return VerticalNavigationSection;
})();

theme.VerticalNavigationSection.prototype = _.assignIn({}, theme.VerticalNavigationSection.prototype, {
  getTopEle: function(ele) {
    return ele.hasClass('nav__item') ? ele : ele.parents('.nav__item');
  },
  onSelect: function(evt) {
    var ele = $(evt.target);
    ele.children('.vertical-navigation__trigger').trigger('click');
  },
  onDeselect: function(evt) {
    var ele = $(evt.target);
    ele.children('.vertical-navigation__trigger').trigger('click');
  },
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    var topEle = this.getTopEle(ele);
    if(topEle.length > 0) {
      topEle.addClass('open');
    }
  },
  onBlockDeselect: function(evt) {
    var ele = $(evt.target);
    var topEle = this.getTopEle(ele);
    if(topEle.length > 0) {
      topEle.removeClass('open');
    }
  }
});

/********* --- Rich Banner Text --- *********/
theme.richBannerText = {};
theme.RichBannerTextSection = (function() {
  function RichBannerTextSection(container) {
    this.wrap = $(container);
    
    BT.initSlider(this.wrap, true, true);
    BT.applyCustomColorSwatches(this.wrap);
    BT.initDealCountdown(this.wrap);
    if(this.wrap.find('.load-ajax').length > 0) {
      var sectionId = this.wrap.attr('data-section-id');
      var wrap = this.wrap;
      var delay = BT.data.cacheWindowWidth >= 992 ? -170 : -370; 
      BT.initScrollingWindowTriggerOnce(this.wrap, 'rich-banner-product-list_' + sectionId, delay, function() {
        wrap.find('.load-ajax').each(function() {
          var grid = $(this);
          BT.callAjax(theme.rootUrl, 'get', {'section_id':sectionId,'view':'ajax'}, null, function(html) {
            var gridSelector = '#' + grid.attr('id');
            grid.html($(gridSelector, html).html());
            if(grid.hasClass('load-ajax-products')) {
              BT.initDealCountdown(grid);
              BT.applyCustomColorSwatches(grid);
              BT.reLoadReview(grid);
              BT.popularAddedWishlistItems(grid);
              BT.convertCurrencySilence(gridSelector + ' span.money');
            }
          });
        });
      });
    }
    
    this.slider = this.wrap.find(BT.getSliderSelector());
  }

  return RichBannerTextSection;
})();

theme.RichBannerTextSection.prototype = _.assignIn({}, theme.RichBannerTextSection.prototype, {
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    if(ele.attr('data-slick-index') != undefined) {
      this.slider.slick('slickGoTo', ele.attr('data-slick-index'), true).slick('slickPause');
    } else {
      if(!BT.isInViewport(ele, evt.currentTarget.defaultView)) {
        $('html, body').animate({
          scrollTop: (ele.offset().top - 100)
        }, 400);
      }
    }
  },
  onBlockDeselect: function() {
    // Resume auto-rotate
    if(this.slider.hasClass('slick-initialized')) {
      this.slider.slick('slickPlay');
    }
  },
  onUnload: function() {
    BT.destroySlider(this.wrap);
    BT.destroyDealCountdown(this.wrap);
    delete this.slider;
    delete this.wrap;
  }
});

/********* --- Layer Banner --- *********/
theme.layerBanner = {};
theme.LayerBannerSection = (function() {
  function LayerBannerSection(container) {
    this.wrap = $(container);
    
    BT.initSlider(this.wrap, true);
    BT.applyCustomColorSwatches(this.wrap);
    BT.initDealCountdown(this.wrap);
    
    this.slider = this.wrap.find(BT.getSliderSelector());
  }

  return LayerBannerSection;
})();

theme.LayerBannerSection.prototype = _.assignIn({}, theme.LayerBannerSection.prototype, {
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    var slider = this.slider;
    var bannerItem = ele.hasClass('banner-item') ? ele : ele.parents('.banner-item');
    if(slider.hasClass('slick-initialized')) {
      if(!ele.hasClass('slick-current') && !ele.parents('.banner-item').hasClass('slick-current')) {
        var newIndex = bannerItem.attr('data-slick-index');
        slider.slick('slickGoTo', newIndex, true);
      }
      slider.slick('slickPause');
    } else {
      if(!BT.isInViewport(bannerItem, evt.currentTarget.defaultView)) {
        var offset = bannerItem.offset().top - 100;
        $('html, body').animate({
          scrollTop: offset
        }, 400);
      }
    }
  },
  onBlockDeselect: function() {
    // Resume auto-rotate
    if(this.slider.hasClass('slick-initialized')) {
      this.slider.slick('slickPlay');
    }
  },
  onUnload: function() {
    BT.destroySlider(this.wrap);
    BT.destroyDealCountdown(this.wrap);
    delete this.slider;
    delete this.wrap;
  }
});

/********* --- Product Tabs --- *********/
theme.productTabs = {};
theme.ProductTabsSection = (function() {
  function ProductTabsSection(container) {
    this.wrap = $(container);
    var sectionId = this.wrap.attr('data-section-id');
    BT.initProductTabs(this.wrap, sectionId);
    BT.applyCustomColorSwatches(this.wrap);
  }

  return ProductTabsSection;
})();

theme.ProductTabsSection.prototype = _.assignIn({}, theme.ProductTabsSection.prototype, {
  onBlockSelect: function(evt) {
    var tab = $(evt.target);
    if(!tab.hasClass('active')) {
      var tabNav = $('a[href="#' + tab.attr('id') + '"]');
      tabNav.trigger('click');
    }
    if(!BT.isInViewport(this.wrap, evt.currentTarget.defaultView)) {
      var offset = this.wrap.offset().top - 100;
      $('html, body').animate({
        scrollTop: offset
      }, 400);
    }
    if(evt.detail.load && tab.hasClass('init-slider-holder')) {
      setTimeout(function() {
        BT.initSlider(tab);
      }, 300);
    }
  },

  onUnload: function() {
    BT.unLoadProductTabs(this.wrap);
    delete this.wrap;
  }
});

/********* --- Simple Product Slider --- *********/
theme.simpleProductSlider = {};
theme.SimpleProductSliderSection = (function() {
  function SimpleProductSliderSection(container) {
    var wrap = $(container);
    this.wrap = wrap;
    var sectionId = wrap.attr('data-section-id');
    this.sectionId = sectionId;
    var delay = wrap.hasClass('load-ajax') ? -470 : 170;
    BT.initScrollingWindowTriggerOnce(wrap, 'simple-product-slider_' + sectionId, delay, function() {
      if(wrap.hasClass('load-ajax')) {
        BT.callAjax(theme.rootUrl, 'get', {'section_id':sectionId,'view':'ajax'}, null, function(html) {
          var gridSelector = '#sps-products-' + sectionId;
          var grid = $(gridSelector)
          grid.html($(gridSelector, html).html());
          BT.initDealCountdown(grid);
          BT.initSlider(wrap, false);
          BT.applyCustomColorSwatches(grid);
          BT.reLoadReview(grid);
          BT.popularAddedWishlistItems(grid);
          BT.convertCurrencySilence(gridSelector + ' span.money');
        });
      } else {
        BT.initDealCountdown(wrap);
        BT.initSlider(wrap, true);
        BT.applyCustomColorSwatches(wrap);
      }
    });
  }

  return SimpleProductSliderSection;
})();

theme.SimpleProductSliderSection.prototype = _.assignIn({}, theme.SimpleProductSliderSection.prototype, {
  onSelect: function(evt) {
    if(evt.detail.load) {
      BT.reLoadReview(this.wrap);
      BT.convertCurrencySilence(this.wrap.find('span.money'));
    }
  },
  onBlockSelect: function(evt) {
    if(!BT.isInViewport(this.wrap, evt.currentTarget.defaultView)) {
      var offset = this.wrap.offset().top - 100;
      $('html, body').animate({
        scrollTop: offset
      }, 400);
    }
  },

  onUnload: function() {
    BT.destroyScrollingWindowTriggerOnce('simple-product-slider_' + this.sectionId);
    delete this.sectionId;
    delete this.wrap;
  }
});

/********* --- Product Columns --- *********/
theme.productColumns = {};
theme.ProductColumnsSection = (function() {
  function ProductColumnsSection(container) {
    this.wrap = $(container);
    if(!this.wrap.hasClass('use-ajax')) {
      BT.convertCurrencySilence(this.wrap.find('span.money'));
      BT.initSlider(this.wrap);
      BT.initDealCountdown(this.wrap);
      BT.applyCustomColorSwatches(this.wrap);
    } else {
      BT.loadProductColumnsAjax(this.wrap);
    }    
  }

  return ProductColumnsSection;
})();

theme.ProductColumnsSection.prototype = _.assignIn({}, theme.ProductColumnsSection.prototype, {
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    if(!BT.isInViewport(ele, evt.currentTarget.defaultView)) {
      var offset = ele.offset().top - 100;
      $('html, body').animate({
        scrollTop: offset
      }, 400);
    }
  },
  onUnload: function() {
    BT.destroySlider(this.wrap);
    BT.destroyDealCountdown(this.wrap);
    delete this.wrap;
  }
});

/********* --- Instagram Slider --- *********/
theme.instagram = {};
theme.InstagramSection = (function() {
  function InstagramSection(container) {
    var $container = $(container);

    BT.initInstagram($('#' + $container.attr('id') + ' .instagram'));
  }

  return InstagramSection;
})();

/********* --- Newsletter Section --- *********/
theme.newsletter = {};
theme.NewsletterSection = (function() {
  function NewsletterSection(container) {
    var $container = $(container);
    BT.initNewsTerms('#' + $container.attr('id'));
  }

  return NewsletterSection;
})();

/********* --- Product Page --- *********/
theme.productTemplate = {};
theme.ProductTemplateSection = (function() {
  function ProductTemplateSection(container) {
    var $container = this.$container = $(container);
    var sectionId = $container.attr('data-section-id');
    var block = this.obj = '#ProductSection-' + sectionId;
    var scriptDataTag = $container.find('.product-single-json');
    if(scriptDataTag.length > 0) {
      BT.data.productGroup[scriptDataTag.attr('data-handle')] = $.parseJSON(scriptDataTag.html());
      BT.addProductRecentView(scriptDataTag.attr('data-handle'));
    }
    BT.data.loadedSnippets.push('preload-product-single-general.scss');
    BT.initProductThumbsSlider(block);
    BT.initProductMainSliderAndZoom(block);
    BT.initDealCountdown(block);
    BT.initStickyCartProductPage();
    BT.applyCustomColorSwatches(block);
    if($('.sc').length > 0) {
      BT.applyCustomColorSwatches('.sc');
    }
    BT.rteWrap(block + ' .rte');

    var upsellWrap = $('.upsell-ele--single');
    if(upsellWrap.length > 0) {
      BT.initScrollingWindowTriggerOnce(upsellWrap, 'upsell_single', -170, function() {
        BT.loadUpsell($container.find(BT.selectors.upsell.wrap));
      });
    }

    var recentPoint = $('.rencent-view-ele-point');
    if(recentPoint.length > 0) {
      BT.initScrollingWindowTriggerOnce(recentPoint, 'recent_view_single', -170, function() {
        BT.loadRecentViewedProductsSingle(recentPoint);
      });
    }

    // BT.calculateMinHeightHover('.related-list');
    var crosssellEle = $('.load-crossell');
    if(crosssellEle.length > 0) {
      BT.loadRecommendationProduct(crosssellEle);
    } else if($('.freb__inner').length > 0) {
      BTFreBought.init(); 
    }
    
    BT.initSizeChart();
    BT.runProgressBar($container.find('.progress-bar'));

    if($('.pg__review-stars--trigger').length > 0) {
      $('.pg__review-stars--trigger').unbind('click');
      $('.pg__review-stars--trigger').on('click',function(e) {
        e.preventDefault();
        var reviewForm = $('.pg__review-form');
        var offsetObj = reviewForm;
        var delay = 0;
        var tabPane = reviewForm.parents('.tab-pane');
        if(tabPane.length > 0) {
          var tabId = tabPane.attr('id');
          offsetObj = $('.nav-tab-item a[href="#' + tabId + '"]');
          if(!tabPane.hasClass('.active')) {
            offsetObj.trigger('click');
          }
          delay = 300;
        }

        if(!BT.isInViewport(offsetObj, window)) {
          var offset = offsetObj.offset().top - 100;
          if($('.use-sticky').length > 0) {
            offset -= 60;
          }
          $('html, body').animate({
            scrollTop: offset
          }, 400);
        }
      });
    }
    
    theme.productTemplate[block] = '#' + $container.attr('id');
  }

  return ProductTemplateSection;
})();

theme.ProductTemplateSection.prototype = _.assignIn({}, theme.ProductTemplateSection.prototype, {
  onSelect: function(evt) {
    if(evt.detail.load) {
      BT.reLoadReview(this.obj);
      BT.alignReviewMasonryForm();
    }
  },
  onBlockSelect: function(evt) {
    var tab = $(evt.target);
    var tabNav = $('a[href="#' + tab.attr('id') + '"]');
    if(!tab.hasClass('active')) {
      tabNav.trigger('click');
    }
    if(!BT.isInViewport(tabNav, evt.currentTarget.defaultView)) {
      var offset = tabNav.offset().top - 100;
      $('html, body').animate({
        scrollTop: offset
      }, 400);
    }
  },
  onUnload: function() {
    delete theme.productTemplate[this.obj];
  }
});

/********* --- Testimonials --- *********/
theme.quotes = {};
theme.QuotesSection = (function() {
  function QuotesSection(container) {
    this.wrap = $(container);
    BT.initSlider(this.wrap, true);
    this.slider = this.wrap.find(BT.getSliderSelector());
  }

  return QuotesSection;
})();

theme.QuotesSection.prototype = _.assignIn({}, theme.QuotesSection.prototype, {
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    if(ele.attr('data-slick-index') != undefined) {
      if(!ele.hasClass('slick-current')) {
        this.slider.slick('slickGoTo', ele.attr('data-slick-index'), true);
      }
      this.slider.slick('slickPause');
    }
  },
  onBlockDeselect: function() {
    // Resume auto-rotate
    if(this.slider.hasClass('slick-initialized')) {
      this.slider.slick('slickPlay');
    }
  },
  onUnload: function() {
    BT.destroySlider(this.wrap);
    delete this.slider;
    delete this.wrap;
  }
});

/********* --- Logo list --- *********/
theme.logoList = {};
theme.LogoListSection = (function() {
  function LogoListSection(container) {
    this.wrap = $(container);
    BT.initSlider(this.wrap, true);
    this.slider = this.wrap.find(BT.getSliderSelector());
  }

  return LogoListSection;
})();

theme.LogoListSection.prototype = _.assignIn({}, theme.LogoListSection.prototype, {
  onBlockSelect: function(evt) {
    var ele = $(evt.target);
    var slideEle = ele.hasClass('slick-slide') ? ele : ele.parents('.slick-slide').first();
    if(!slideEle.hasClass('slick-current')) {
      this.slider.slick('slickGoTo', slideEle.attr('data-slick-index'), true);
    }
    this.slider.slick('slickPause');
  },
  onBlockDeselect: function() {
    // Resume auto-rotate
    if(this.slider.hasClass('slick-initialized')) {
      this.slider.slick('slickPlay');
    }
  },
  onUnload: function() {
    BT.destroySlider(this.wrap);
    delete this.slider;
    delete this.wrap;
  }
});

/********* --- Single Deal --- *********/
theme.singleDeal = {};
theme.SingleDealSection = (function() {
  function SingleDealSection(container) {
    this.wrap = $(container);
    var sectionId = this.wrap.attr('data-section-id');
    this.suffixEvent = 'single_deal_' + sectionId;

    BT.initScrollingWindowTriggerOnce(this.wrap, this.suffixEvent, -170, function() {
      BT.initDealCountdown(this.wrap);
    }.bind(this));
  }

  return SingleDealSection;
})();

theme.SingleDealSection.prototype = _.assignIn({}, theme.SingleDealSection.prototype, {
  onSelect: function() {
  },
  onUnload: function() {
    BT.destroyScrollingWindowTriggerOnce(this.suffixEvent);
    BT.destroyDealCountdown(this.wrap);
    delete this.suffixEvent;
    delete this.wrap;
  }
});

/********* --- About section --- *********/
theme.about = {};
theme.AboutSection = (function() {
  function AboutSection(container) {
    this.wrap = $(container);
    BT.initSlider(this.wrap, true);
  }

  return AboutSection;
})();

theme.AboutSection.prototype = _.assignIn({}, theme.AboutSection.prototype, {
  onSelect: function() {
  },
  onUnload: function() {
    delete this.wrap;
  }
});

/********* --- Footer --- *********/
theme.footer = {};
theme.FooterSection = (function() {
  function FooterSection(container) {
    var $container = this.$container = $(container);
    var sectionId = $container.attr('data-section-id');
    var block = this.obj = '#' + sectionId;

    if($('.social-app').length > 0) {
      $('.social-app').each(function() {
        var ele = $(this);
        var suffixEvent = 'social_app_' + ele.attr('data-block-id');
        BT.initScrollingWindowTriggerOnce(ele, suffixEvent, -270, function() {
          if(ele.hasClass('social-app--fb')) {
            var dataAppId = ele.attr('data-app-id');
            if(dataAppId) {
              var url = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=" + dataAppId;
              (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = url;
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            }
          } else {
            var script = document.createElement('script');
            script.src = "//platform.twitter.com/widgets.js";
            script.charset = "utf-8";
            script.async = true;
            document.getElementsByTagName('head')[0].appendChild(script);
          }
          setTimeout(function() {ele.removeClass('loading');},1000);
        });
      });
    }

    BT.loadProductColumnsAjax($container);

    if(theme.newsTerms && $container.find('.newsletter--footer').length > 0) {
      $container.find('.newsletter--footer').each(function() {
        BT.initNewsTerms('#' + $(this).attr('id'));
      });
    }
 
    theme.footer[block] = '#' + $container.attr('id');
  }
  return FooterSection;
})();

theme.FooterSection.prototype = _.assignIn({}, theme.FooterSection.prototype, {
  onSelect: function() {
  },
  onUnload: function() {
    delete theme.footer[this.obj];
  }
});