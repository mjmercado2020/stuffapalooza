var BTCollection = {
  selectors: {
    filter: ".cf",
    filterLink: ".cf__link",
    filterLinkColor: ".cf__link--color",
    products: ".collection__products",
    currentTags: ".collection__current-tags",
    productsInner: ".products",
    modeLink: ".ct__mode__link",
    sort: ".ct__sort",
    total: ".ct__total",
    pagination: ".pagination",
    scrollBtn: ".collection__scroll",
    paginationButton: ".collection__scroll,.button--more,.pagination",
    productGrid: ".grid--products",
    productItem: ".grid__item",
    recentViewWrap: ".cf__item--recent-view",
    recentViewList: ".cf__item--recent-view__content",
    wrap: "#collection-template"
  },

  options: {
    clickEvent: "click",
    ajaxView: "ajax"
  },

  data: {},

  ignoreViewParam: function(str) {
    if (str != "" && str != undefined) {
      str = str
        .replace(/\?view=ajax\"/g, '"')
        .replace(/\?view=ajax\&/g, "?")
        .replace(/\&view=ajax/g, "");
    }
    return str;
  },

  updateHtml: function(html, updateAdditionalInfo) {
    var selector = this.selectors;
    var filterStr = $(selector.filter, html).html();
    $(selector.filter).html(this.ignoreViewParam(filterStr));
    var newGrid = $(selector.productGrid, html);
    $(selector.productGrid).html(newGrid.html());
    if (newGrid.hasClass("grid--full")) {
      $(selector.productGrid).addClass("grid--full");
    } else {
      $(selector.productGrid).removeClass("grid--full");
    }
    if (updateAdditionalInfo) {
      $(".breadcrumbs-page-title").html(
        $(".breadcrumbs-page-title", html).html()
      );
      $(".breadcrumbs").html($(".breadcrumbs", html).html());
      $(".collection__top").html($(".collection__top", html).html());
    }
    $(selector.currentTags).html(
      this.ignoreViewParam($(selector.currentTags, html).html())
    );
    BT.reLoadReview(selector.productGrid);
    BtCurrency.convertSilence(
      shopCurrency,
      BtCurrency.currentCurrency,
      $(selector.wrap).find("span.money")
    );
    BT.applyCustomColorSwatches(selector.products);
    BT.initDealCountdown(selector.products);
    BT.popularAddedWishlistItems(selector.products);
    this.applyColorSwatchesContrast();
    $(selector.total).html($(selector.total, html).html());

    var collectionContent = $(".collection__content");
    if (collectionContent.offset().top < $(window).scrollTop()) {
      $("html, body").animate(
        {
          scrollTop: collectionContent.offset().top - 100
        },
        400
      );
    }

    // Update sort by
    if ($(this.selectors.sort).length > 0) {
      $(this.selectors.sort).html($(this.selectors.sort, html).html());
      $(this.selectors.sort).removeClass("active");
    }

    // Update scroll button
    if ($(selector.paginationButton, html).length > 0) {
      if ($(selector.paginationButton).length > 0) {
        $(selector.paginationButton).remove();
      }
      $(selector.products).append(
        $(selector.paginationButton, html).get(0).outerHTML
      );
      if ($(selector.scrollBtn).length > 0) {
        this.initInfiniteScrollCollection(selector.products);
      }
    } else if ($(selector.paginationButton).length > 0) {
      if ($(selector.scrollBtn).length > 0) {
        this.destroyInfiniteScroll();
      }
      $(selector.paginationButton).remove();
    }
  },

  initAjaxLinkEvent: function() {
    var ins = this;
    $(document).on(this.options.clickEvent, this.selectors.filterLink, function(
      e
    ) {
      e.preventDefault();
      BT.showLoadingFull();
      var link = $(this);
      var href = link.attr("href");
      BT.callAjax(href, "GET", { view: ins.options.ajaxView }, null, function(
        html
      ) {
        ins.updateHtml(html, link.hasClass("cfc__link"));
        BT.hideLoadingFull();

        // Update history
        try {
          var newUrl =
            window.location.protocol +
            "//" +
            window.location.host +
            ins.ignoreViewParam(href);
          window.history.replaceState({ path: newUrl }, "", newUrl);
        } catch (e) {
          console.log(e);
        }
      });
    });
  },

  initViewModeEvent: function() {
    var ins = this;
    $(document).on(this.options.clickEvent, this.selectors.modeLink, function(
      e
    ) {
      e.preventDefault();
      if ($(this).hasClass("active")) {
        return;
      }
      var oldActive;
      if (
        $(this)
          .parent()
          .hasClass("ct__mode__grid-mobile")
      ) {
        oldActive = $(".ct__mode__grid-list");
        var desktopMode = $(this).attr("data-mode");
        $('.ct__mode__grid-desktop [data-mode="' + desktopMode + '"]')
          .parent()
          .addClass("active");
      } else {
        oldActive = $(this)
          .parent()
          .siblings(".active:not(.ct__mode__grid-mobile)");
        if (
          $(this)
            .parent()
            .hasClass("ct__mode__grid-desktop")
        ) {
          $(".ct__mode__grid-mobile")
            .addClass("active")
            .children()
            .attr("data-mode", $(this).attr("data-mode"));
        } else {
          $(".ct__mode__grid-mobile").removeClass("active");
        }
      }
      var oldMode = oldActive
        .children(ins.selectors.modeLink)
        .attr("data-mode");
      oldActive.removeClass("active");
      $(this)
        .parent()
        .addClass("active");
      $(ins.selectors.products)
        .find(ins.selectors.productGrid)
        .removeClass(oldMode)
        .addClass($(this).attr("data-mode"));
    });
  },

  destroyInfiniteScroll: function() {
    BT.destroyInfiniteScroll("collection-template");
  },

  initInfiniteScrollCollection: function(wrap) {
    this.destroyInfiniteScroll();
    BT.initInfiniteScroll(wrap);
  },

  openCurrentFilterDropdown: function() {
    $(".cfc__link.active").each(function() {
      var link = $(this);
      link.parents(".cfc__dropdown").each(function() {
        var dropdown = $(this);
        dropdown.show();
        dropdown.parent(".link-list__item").addClass("open");
      });
    });
  },

  applyColorSwatchesContrast: function() {
    if ($(this.selectors.filterLinkColor).length > 0) {
      $(this.selectors.filterLinkColor).each(function() {
        var link = $(this);
        var bkgColor = link.children().attr("data-bkg");
        if (bkgColor != undefined) {
          var newBkgColor = BT.invertColor(bkgColor, true);
          if (newBkgColor) {
            link.css("color", newBkgColor);
          }
        }
      });
    }
  },

  loadRecentViewProducts: function() {
    var wrap = $(this.selectors.recentViewWrap);
    if (wrap.length > 0) {
      var list = $(this.selectors.recentViewList);
      var handleItems = BT.getCookieItemsValue(
        true,
        BT.options.recentView.cookieName
      );
      if (handleItems.length > 0) {
        handleItems.reverse();
        var q = handleItems.join(",");
        BT.callAjax(
          theme.searchUrl,
          "GET",
          { view: "recent_view_list", q: q },
          null,
          function(html) {
            list.html(html);
            if (list.find(".item").length > 0) {
              BT.convertCurrencySilence(list.find("span.money"));
              BT.reLoadReview(list);
              wrap.removeClass("hide");
            }
          }
        );
      }
    }
  },

  init: function() {
    if ($("#collection__filter--mobile").length > 0) {
      $("#collection__filter--mobile").html($("#cf_filter_desktop").html());
    }
    this.initAjaxLinkEvent();
    this.initViewModeEvent();
    BT.initExpandTrigger();
    this.applyColorSwatchesContrast();
    this.loadRecentViewProducts();
    // this.openCurrentFilterDropdown();
  }
};

theme.collectionTemplate = {};
theme.CollectionTemplateSection = (function() {
  function CollectionTemplateSection(container) {
    var $container = (this.$container = $(container));
    this.obj = "#" + $container.attr("data-section-id");
    BtCurrency.convertSilence(
      shopCurrency,
      BtCurrency.currentCurrency,
      $container.find("span.money")
    );
    BTCollection.initInfiniteScrollCollection($container);
    BT.applyCustomColorSwatches(
      $container.find(BTCollection.selectors.products)
    );
    BT.initDealCountdown($container.find(BTCollection.selectors.products));
  }
  return CollectionTemplateSection;
})();

theme.CollectionTemplateSection.prototype = _.assignIn(
  {},
  theme.CollectionTemplateSection.prototype,
  {
    onSelect: function(evt) {
      if (evt.detail.load) {
        BT.cloneSameContent(this.obj);
        // BTCollection.openCurrentFilterDropdown();
      }
    },
    onUnload: function() {
      BTCollection.destroyInfiniteScroll();
    }
  }
);

$(document).ready(function() {
  BTCollection.init();
  theme.sections.register(
    "collection-template",
    theme.CollectionTemplateSection
  );
});
