(function($) {
  $(document).ready(function() {
    $(".tab-accordion__trigger").click(function(e) {
      e.preventDefault();
      var $this = $(this),
        id = $this.attr("href"),
        content = $(`.tab-pane--accordion[id='${id}']`);
      content.slideToggle();
    });
  });
})(jQuery);
