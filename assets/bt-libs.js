var BT = {
  selectors: {
    sidebar : {
      btn: '.bt-sidebar-btn',
      box: '.bt-sidebar',
      closeBtnCanvas: '.bt-sidebar__close',
      closeSidebarBtn: '.close-menu-btn',
      openClass: 'open-sidebar-canvas'
    },
    overlay: '.overlay',
    slider : {
      useTouchMobile: 'use-touch-mobile',
      default: '.use-slider',
      overFloatClasses: 'ov-slider-tb ov-slider-mb hide-in-loading'
    },
    desktopNav: {
      topItem: '.nav__item',
      parentItem: '.nav__item--parent',
      dropdown: '.nav__item__dropdown',
      parentNav: '.header__wrap'
    },
    verticalNav: {
      topItem: '.nav__item--ver',
      parentItem: '.nav__item--parent',
      dropdown: '.nav__item__dropdown',
      parentNav: '.main-nav--ver'
    },
    dropdown: {
      wrap: ".bt-dropdown",
      trigger: ".bt-dropdown__trigger",
      content: ".bt-dropdown__content"
    },
    productGroup: {
      wrap: ".pg",
      option: ".pg__option",
      swatch: ".pg__option__values",
      optionValue: ".pg__option__value",
      mainImg: '.pg__main-image',
      thumbs: '.pg__thumbs',
      thumbsSmallClass: 'pg__thumbs--small',
      thumbsLargeClass: '.pg__thumbs--large',
      thumbsSlider: '.pg__thumbs--slider',
      thumbsSliderVerticalClass: 'pg__thumbs--slider-vertical',
      thumb: '.pg__thumb',
      thumbLink: '.pg__thumb__link',
      mains: '.pg__mains',
      mainsSlider: '.pg__mains--slider',
      main: '.pg__main',
      mainLink: '.pg__main__link',
      price: '.product-price',
      cartBtn: '.add-cart-btn',
      cartBtnState: '.add-cart-btn--state',
      variantId: '.variant-id',
      variantTitle: '.pg__variant-title',
      loadingClass: 'loading-data',
      groupData: '.group-data',
      isGroupClass: 'is-group',
      reCalcIndexClass: 're-calc-index',
      imgZoom: '.cloudzoom',
      countdown: '.pg__countdown-js',
      syncUrl: '.pg__sync-url',
      labelDeal: '.lb-item--sale',
      labelPercent: '.lb__percent',
      stickySmallPrice: '.sc__small-price',
      stockText: '.pg__stock-text',
      stockText1: '.pg__stock-text1',
      stockText2: '.pg__stock-text2',
      stockNumber: '.pg__stock-number--update',
      video: '.pg__video',
      videoYoutube: '.pg__video--youtube',
      videoVimeo: '.pg__video--vimeo',
      sku: '.pg__sku'
    },
    cart: {
      addBtn: '.add-cart-btn',
      updateBtn: '.update-cart-btn',
      header: '.header__cart',
      headerTriggerDesktop: '.header__cart-trigger-desktop',
      headerTriggerMobile: '.header__cart-trigger-mobile',
      number: '.cart-number',
      total: '.cart-total',
      selectOptionClass: 'select-option',
      qtyInput: '.cart__qty-input'
    },
    qtyBox: {
      ctrlBtn: '.qty-box__ctrl',
      decreaseBtnClass: 'qty-box__ctrl--descrease',
      increaseBtnClass: 'qty-box__ctrl--increase',
      inputSyncClass: 'qty-box__input--sync'
    },
    mediaBtn: '.button--media',
    loadingAjaxClass: 'loading-ajax',
    workingClass: 'working',
    qvBtn: '.button--qv',
    qvPopup: '#quickViewPopup',
    selectOptionBtn: '.button--select-option',
    selectOptionPopup: '#selectOptionPopup',
    compare: {
      popup: '#comparePopup',
      btn: '.button--cp',
      removeBtn: '.button-cp--remove',
      variantId: '.variant-id--compare',
      actionsRow: '.actions-row',
      availabilityRow: '.availability-row',
      bestPriceBadge: '.compare-popup__badge',
      headerLink: '.header__link--compare'
    },
    wishlist: {
      popup: '#wishlistPopup',
      btn: '.button--wl',
      removeBtn: '.button-wl--remove',
      headerLink: '.header__link--wishlist'
    },
    waitingData: '.waiting-data', // ignore this element when init slider to wait the data is loaded.
    waitingTabData: '.waiting-tab-data', // load data when the correspoding tab is opened.
    waitingScrollData: '.waiting-scroll-data', // load data when element is visible in viewport.
    loadMoreBtn: '.button--more',
    loadingFull: '.loading-full',
    cartMessage: '.cart-message',
    cartModal: {
      wrap: '#cartModal',
      productImage: '.cart-response__image',
      productName: '.cart-response__name',
      variantTitle: '.cart-response__meta-variant-title',
      variantProp: '.cart-response__meta-propeties',
      price: '.cart-response__price .product-price__price',
      linePrice: '.cart-response__line-price .product-price__price',
      qty: '.cart-response__qty',
      freeShippingText: '.cart-response__free-shipping__text',
      freeShippingProgressBar: '.progress-bar--popup-cart'
    },
    notify: '.notify',
    notifyClassTypes: 'alert-success alert-danger',
    floatField: '.field--float-label',
    floatFieldFirstFocus: '.first-focus',
    /* Recommended products */
    rp: {
      wrap: '.rp',
      item: '.rp__item',
      minuteNumber: '.rp__minute-number',
      closeBtn: '.rp__close'
    },
    /* Newsletter popup */
    newsPopup: {
      id: '#newsletterPopup',
      checkbox: '#newsPopupHide'
    },
    newsTerms: {
      form: '.newsletter-form',
      input: '.news-terms-input',
      error: '.news-terms-error'
    },
    /* Promotion */
    promo: {
      wrap: '.promo',
      closeBtn: '.promo__close'
    },
    upsell: {
      wrap: '.upsell-ele',
      list: '.upsell-list'
    },
    related: {
      wrap: '.related-ele',
      list: '.related-list',
    },
    loadingNotFull: '.loading-not-full',
    infiniteScroll: {
      button: '.infinite-scroll',
      wait: '.wait-infinite-scroll'
    },
    cartTerms: {
      wrap: '.cart-terms',
      popup: '#termsPopup',
      checkbox: '.terms-check',
      checkoutBtn: '.checkout-btn',
      paymentBtn: '.shopify-payment-button,.additional-checkout-buttons'
    },
    popupVideo: {
      btn: '.video-thumbnail',
      youtubeBtn: '.js-modal-btn--youtube',
      vimeoBtn: '.js-modal-btn--vimeo'
    },
    searchFull: {
      wrap: '.search-full',
      trigger: '.search-full-trigger',
      content: '.search-full__content',
      input: '.search-full__input',
      loading: '.search-full__loading',
      close: '.search-full__close-js',
      tabTitle: '.search-full__tab-title',
      tabList: '.search-full__tab-list',
      tabContent: '.search-full__content__tab',
      result: '.search-full__result',
      loadedClass: 'loaded'
    },
    ajaxSnippet: '.load-ajax-snippet'
  },
  options: {
    dropdown: {
      duration: 150
    },
    sidebar: {
      duration: 300,
      openEvent: 'touchend click',
      closeEvent: 'touchstart click'
    },
    slider: {
      slidesToShow: 4,
      slidesToScroll: 1,
      //asNavFor: wrap + ' .viewmore-main',
      dots: false,
      //centerMode: true,
      focusOnSelect: false,
      infinite: false,
      swipe: true,
      swipeToSlide: true,
      rtl: rtl,
      nextArrow: '<button aria-label="button" class="slick-next button button--style2 button--circle lh1"><span></span></button>',
      prevArrow: '<button aria-label="button" class="slick-prev button button--style2 button--circle lh1"><span></span></button>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    },
    productGroup: {
      smallSlider: {
        slidesToShow: 5,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: false,
        focusOnSelect: true,
        centerMode: false,
        rtl: rtl,
        nextArrow: '<button aria-label="button" class="slick-next button button--style2 button--circle lh1 slick-box"><span></span></button>',
        prevArrow: '<button aria-label="button" class="slick-prev button button--style2 button--circle lh1 slick-box"><span></span></button>',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              "vertical":false,
              "verticalSwiping":false
            }
          }
        ]
      },
      mainSlider: {
        slidesToShow: 1,
        slidesToScroll: 1,
        //asNavFor: wrap + ' .viewmore-thumbs',
        arrows: false,
        fade: true,
        infinite: false,
        swipeToSlide: true,
        arrows: true,
        adaptiveHeight: true,
        lazyLoad: 'ondemand',
        rtl: rtl,
        nextArrow: '<button aria-label="button" class="slick-next button button--style2 button--circle lh1 slick-box"><span></span></button>',
        prevArrow: '<button aria-label="button" class="slick-prev button button--style2 button--circle lh1 slick-box"><span></span></button>',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              "dots":true,
              "arrows":false
            }
          }
        ]
      }
    },
    windowScreen: {
      desktop: 768,
      mobile: 767
    },
    productZoom: {zoomSizeMode: 'image',autoInside:true, tintOpacity: 0, zoomPosition: 'inside'},
    countdown: {
      dealCountdownReg : '([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})',
      dealCountdownTpl : $('#bt_countdown_html').html()
    },
    compare: {
      cookieName: 'bt-compare',
      firstLoaded: false
    },
    wishlist: {
      cookieName: 'bt-wishlist'
    },
    productNameHolder: 'product_title',
    notifyDuration: 300,
    notifyTimeout: 3000,
    notifyClassTypes: {
      success: 'success',
      danger: 'danger'
    },
    rp: {
      cookieName: 'rp_hide',
      duration: 700
    },
    newsPopup: {
      cookieName: 'hide_news_popup'
    },
    promo: {
      cookieName: 'hide_promo'
    },
    cartTerms: {
      cookieName: 'cart_terms'
    },
    newsTerms: {
      cookieName: 'news_terms'
    },
    recentView: {
      cookieName: 'recent_view_products'
    }
  },
  data: {
    productGroup: {},
    productVideo: {
      currentPlayer: null,
      currentType: null,
      loadYoutubeApi: false,
      loadVimeoApi: false,
      vimeoPlayers: {}
    },
    productGroupInfiniteScroll: {},
    updateCartRequests: [],
    favicon: null,
    zoom: {
      single: null,
      quickView: null
    },
    rp: {
      openTimeInterval: null
    },
    modalVideoWorking: false,
    loadedSnippets: [],
    thresholdMenu: 0,
    stickyMenuObj: null,
    resizeEventName: 'bt_resize_window',
    scrollEventName: 'bt_scroll_window',
    cacheWindowWidth: $(window).width(),
    isTouchDevide: window.matchMedia("(any-pointer: coarse)").matches,
    thresholdMenuWithPromotion: 33,
    sliderIndex: 0,
    wishlist: {
      loaded: false
    },
    cookieArray: {},
    pageLoaded: false,
    searchSuggestionCurrentType: 'product',
    searchSuggestionCurrentAjaxRequest: null,
    customColorCodes: null,
    customColorCodesLoading: false,
    customColorCodesStack: [],
    initTopBar: false
  },

  shuffleArray: function (d) {
    for (var c = d.length - 1; c > 0; c--) {
      var b = Math.floor(Math.random() * (c + 1));
      var a = d[c];
      d[c] = d[b];
      d[b] = a;
    }
    return d;
  },

  /**
   * Returns a random integer between min (inclusive) and max (inclusive)
   * Using Math.round() will give you a non-uniform distribution!
   */
  getRandomInt: function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  getCookie: function (name) {
    try {
      return $.cookie(name);
    } catch(e) {
    }
  },

  setCookie: function(name, value, expire) {
    try {
      var params = {path: '/'};
      if(expire) {
        params.expires = expire;
      }
      $.cookie(name, value, params);
    } catch(e) {
    }
  },

  stripScripts: function(s) {
    var div = document.createElement('div');
    div.innerHTML = s;
    var scripts = div.getElementsByTagName('script');
    var i = scripts.length;
    while (i--) {
      scripts[i].parentNode.removeChild(scripts[i]);
    }
    return div.innerHTML;
  },

  callAjax: function(url, type, data, dataType, cb, cbError) {
    var ins = this;
    return $.ajax({
      type: type,
      url: url,
      data: data,
      dataType: dataType,
      error: function(xhr, textStatus, errorThrown) {
        ins.hideLoadingFull();
        if(xhr.responseJSON != undefined) {
          if(xhr.responseJSON.description != undefined && xhr.responseJSON.description != null) {
            ins.showNotify(xhr.responseJSON.description, ins.options.notifyClassTypes.danger);
          } else if (xhr.responseJSON.message != undefined) {
            ins.showNotify(xhr.responseJSON.message, ins.options.notifyClassTypes.danger);
            //showNotifyMsg(xhr.responseJSON.message, 'error');
          }
        }
        if(cbError) {
          cbError();
        }
      },
      success: function(response){
        cb(response);
      }
    });
  },

  requestUpdateCart: function(line, qty, cb, cbError){
    $(this.data.updateCartRequests).each(function(index, xhr) {
      if(xhr && xhr.readyState != 4){
        xhr.abort();
      }
    });
    this.data.updateCartRequests = [];
    this.data.updateCartRequests.push(this.callAjax(theme.cartChangeUrl, 'POST', {line: line, quantity: qty}, 'json', function(cart) {
      if(cb) {
        cb(cart);
      }
    }, cbError));
  },

  requestEmptyCart: function(cb){
    this.callAjax(theme.cartClearUrl, 'POST', {}, 'json', function(cart) {
      if(cb) {
        cb(cart);
      }
    });
  },

  resizeImage : function(t, r) {
    try {
        if ("original" == r) return t;
        var e = t.match(/(.*\/[\w\-\_\.]+)\.(\w{2,4})/);
        return e[1] + "_" + r + "." + e[2]
    } catch (r) {
        return t
    }
  },

  isInViewport : function(ele, windowEle) {
      var elementTop = ele.offset().top;
      var elementBottom = elementTop + ele.outerHeight();
      var viewportTop = $(windowEle).scrollTop();
      var viewportBottom = viewportTop + $(windowEle).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
  },

  rteWrapTable: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    //
    wrap.find('table').each(function() {
      $(this).removeAttr('style').removeAttr('border').addClass('table table-bordered table-striped').wrap(
        '<div class="table-responsive"></div>'
      );
    });
  },

  rteWrapEmbed: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    wrap.find('.embed-responsive-item,iframe,embed,object,video').each(function() {
      /*$(this).wrap(
        '<div class="embed-responsive"></div>'
      );
      this.src = this.src;*/
      $(this).css('max-width','100%');
    });
  },

  rteWrap: function(wrap) {
    this.rteWrapTable(wrap);
    this.rteWrapEmbed(wrap);
  },

  startLoadingBtn: function(btn) {
    btn.find('i').addClass(this.selectors.loadingAjaxClass);
  },

  endLoadingBtn: function(btn) {
    btn.find('i').removeClass(this.selectors.loadingAjaxClass);
  },

  showLoadingFull: function() {
    if(!this.data.initTopBar) {
      topbar.config(topbarConfig);
      this.data.initTopBar = true;
    }
    topbar.show();
  },

  hideLoadingFull: function() {
    topbar.hide();
  },

  getPriceHtml: function(price) {
    return theme.Currency.formatMoney(price, theme.moneyFormat);
  },

  handleString: function(str) {
    return str.toLowerCase().replace(/\"/g,'').replace(/\'/g,'').replace(/[\(,\*,\)]/g,'').replace(/  /g,' ').replace(/ /g,'-');
  },

  convertCurrencySilence: function(selector) {
    BtCurrency.convertSilence(shopCurrency, BtCurrency.currentCurrency, selector);
  },

  callSnippetAjax: function(snippetName, cb) {
    this.callAjax(theme.searchUrl, 'GET', {view: 'snippet', q: snippetName}, null, function(html) {
      cb(html);
    });
  },

  loadSnippetAjax: function(snippetName, cb, delay) {
    if(this.data.loadedSnippets.indexOf(snippetName) > -1) {
      cb();
    } else {
      var ins = this;
      this.callSnippetAjax(snippetName, function(html) {
        ins.data.loadedSnippets.push(snippetName);
        if($('.snippet-error', html).length == 0) {
          $('body').append(html);
          setTimeout(function() {
            cb();
          }, (delay) ? delay : 300);
        }
      });
    }
  },

  loadAjaxSnippetByEvent: function(snippetName) {
    if(this.data.loadedSnippets.indexOf(snippetName) == -1) {
      var ins = this;
      this.callSnippetAjax(snippetName, function(html) {
        if($('.snippet-error', html).length == 0) {
          $(ins.selectors.ajaxSnippet + '[data-snippet="' + snippetName + '"]').each(function() {
            ins.data.loadedSnippets.push(snippetName);
            $(this).html(ins.processSameId(html));
            $(this).removeClass(ins.selectors.ajaxSnippet.replace('.','')).removeAttr('data-snippet');
            if(snippetName == 'header-cart-content') {
              ins.convertCurrencySilence($(this).find('span.money'));
              ins.resetCartTerms();
              if (window.Shopify && Shopify.StorefrontExpressButtons && !$('body').hasClass('template-cart')) {
                Shopify.StorefrontExpressButtons.initialize();
              }
            }
          });
        }
      });
    }
  },

  runLoadAjaxSnippetWithEvent: function(ele) {
    var ajaxSnippets = ele.find(this.selectors.ajaxSnippet);
    var ins = this;
    if(ajaxSnippets.length > 0) {
      ajaxSnippets.each(function() {
        ins.loadAjaxSnippetByEvent($(this).attr('data-snippet'));
      });
    }
  },

  processSameId: function(htmlStr) {
    var cloneIdElements = htmlStr.match(/data-clone-id=\"(.*?)\"/g);
    if(cloneIdElements != null) {
      $.each(cloneIdElements, function(index, value){
        var strFind = value.replace(/data-clone-id=/g,'').replace(/"/g,'');
        var newId = 'clone-' + Math.floor(Math.random() * 100);
        var regex = new RegExp( strFind, 'g' );
        htmlStr = htmlStr.replace(regex, newId);
      });
    }
    return htmlStr;
  },

  showPopup: function(id) {
    var timeout = 0, ins = this, popupClass = id.replace('#','');
    if($('.modal.in:not(.' + popupClass + ')').length > 0) {
      $('.modal.in:not(.' + popupClass + ')').each(function() {
        ins.hidePopup('#' + $(this).attr('id'));
      });
      timeout = 500;
    }
    setTimeout(function() {
      $(id).modal('show');
    }, timeout);
  },

  hidePopup: function(id) {
    $(id).modal('hide');
  },

  showNotify: function(msg, type) {
    var notify = $(this.selectors.notify), activeClass = 'active', ins = this;
    notify.removeClass(this.selectors.notifyClassTypes);
    notify.addClass('alert-' + type);
    notify.html(msg);
    if(notify.hasClass(activeClass)) {
      if(this.data.notifyTimeout) {
        clearTimeout(this.data.notifyTimeout);
      }
      this.hideNotify();
    } else {
      notify.slideDown(ins.options.notifyDuration, function() {
        notify.addClass(activeClass);
        ins.hideNotify();
      }); 
    }
  },

  hideNotify: function() {
    var notify = $(this.selectors.notify), activeClass = 'active', ins = this;
    this.data.notifyTimeout = setTimeout(function() {
      notify.slideUp(ins.options.notifyDuration, function() {
        notify.removeClass(activeClass);
      });
    }, this.options.notifyTimeout);
  },

  runLoadingDropdownNavInAjax: function(wrap, type, cb) {
    var sectionId = $(wrap).attr('data-section-id');
    var dropdownSelector = this.selectors[type].dropdown;
    this.callAjax(theme.rootUrl, 'get', {'section_id':sectionId, 'view': 'ajax'}, null, function(html){
      $(wrap).find(dropdownSelector).each(function(){
        var dropdownId = '#' + $(this).attr('id');
        $(this).html($(dropdownId, html).html());
      });
      cb();
    });
  },

  /* Desktop Navigation */
  initDesktopNavigation: function(wrap, type) {
    var ins = this;
    var selectors = ins.selectors[type], curWinWidth = this.data.cacheWindowWidth, overlaySelector = this.selectors.overlay;
    var loadedDropdown, resetDropdownStatus = false, resetWithData = false;
    function resetDropdown(wrap) {
      if(!resetDropdownStatus && ins.data.cacheWindowWidth >= ins.options.windowScreen.desktop) {
        resetDropdownStatus = true;
        $(wrap + ' ' + selectors.topItem).each(function() {
          if($(this).find(selectors.dropdown + ' > .row > div').length > 0) {
            $(this).addClass(selectors.parentItem.replace('.', ''));
            $(this).find(selectors.dropdown).removeClass('hide');
          }
        });
      }
    }
    function alignDropdown(wrap) {
      if(type == 'desktopNav') { // Horizontal navigation
        if(ins.data.cacheWindowWidth >= ins.options.windowScreen.desktop && loadedDropdown == true) {
          var parentItems = $(wrap + ' ' + selectors.parentItem);
          if(parentItems.length > 0) {
            var parentNav = $(selectors.parentNav);
            var thresholdLeft = parentNav.offset().left;
            var thresholdRight = thresholdLeft + parentNav.outerWidth();
            parentItems.each(function() {
              var dropdown = $(this).children(selectors.dropdown);
              dropdown.css({ 'left' : '', 'right' : '' });
              var centerPoint = $(this).offset().left + $(this).outerWidth()/2;
              var dropdownWidth = dropdown.outerWidth();
              var dropdownHalf = dropdownWidth/2;
              if(!rtl) {
                var leftPoint = $(this).offset().left;
                if(leftPoint + dropdownWidth > thresholdRight) {
                  if(centerPoint + dropdownHalf < thresholdRight) {
                    if(centerPoint - dropdownHalf > thresholdLeft) {
                      var left = centerPoint - dropdownHalf - thresholdLeft - 15;
                      dropdown.css({'left': left});
                    } else {
                      dropdown.css({'left': 0});
                    }
                  } else {
                    dropdown.css({'right': 0, 'left': 'auto'});
                  }
                } else {
                  dropdown.css({'left': (leftPoint - thresholdLeft - 15)});
                }
              } else {
                var rightPoint = $(this).offset().left + $(this).outerWidth();
                if(rightPoint - dropdownWidth < thresholdLeft) {
                  if(centerPoint - dropdownHalf > thresholdLeft) {
                    if(centerPoint + dropdownHalf > thresholdRight) {
                      dropdown.css({'left': 0});
                    } else {
                      var left = centerPoint - dropdownHalf - thresholdLeft + 15;
                      dropdown.css({'left': left});
                    }
                  } else {
                    dropdown.css({'left': 0, 'right': 'auto'});
                  }
                } else {
                  dropdown.css({'left': (rightPoint - thresholdLeft - dropdownWidth + 15)});
                }
              }
            });
          }
        }
      } else { // Vertical navigation
        if(ins.data.cacheWindowWidth >= ins.options.windowScreen.desktop) {
          var parentItems = $(wrap + ' ' + selectors.parentItem + ' > a');
          if(parentItems.length > 0) {
            var parentNav = $(selectors.parentNav);
            var thresholdTop = parentNav.offset().top;
            parentItems.each(function() {
              var dropdown = $(this).siblings(selectors.dropdown);
              dropdown.css("top","");
              var centerPoint = $(this).offset().top + $(this).outerHeight()/2;
              var dropdownHalf = dropdown.outerHeight()/2;
              if(centerPoint - dropdownHalf > thresholdTop) {
                var top = centerPoint - dropdownHalf - thresholdTop;
                dropdown.css({'top': top});
              } else {
                dropdown.css({'top': 0});
              }
            });
          }
        }
      }
    }
    function hideOpeningMenu(wrap) {
      $(wrap + ' .nav__item--parent.hover').removeClass('hover');
      $(wrap + ' .nav__item--parent.clicked').removeClass('clicked');
    }
    function initHoverMenu(wrap) {
      if(type != 'desktopNav') { // Init trigger button in the vertical navigation
        $('.vertical-navigation__trigger').unbind('click');
        $('.vertical-navigation__trigger').on('click',function(e) {
          e.preventDefault();
          $(this).toggleClass('open');
          $(this).siblings('.vertical-navigation__inner').toggleClass('open');
        });
      }
      $(document).on('mouseover', wrap + ' .nav__item--parent:not(hover),' + wrap + ' .link-list__item--parent:not(hover)', function(e) {
        e.preventDefault();
        $(this).siblings('.hover').removeClass('hover');
        $(this).addClass('hover');
      });

      $(document).on('mouseout', wrap + ' .nav__item--parent.hover,' + wrap + ' .link-list__item--parent.hover', function(e) {
        e.preventDefault();
        $(this).removeClass('hover');
      });
      
      $(document).on('click', 'body', function(e) {
        if($(e.target).parents(wrap + ' .nav__item--parent').length == 0) {
          $(wrap + ' .nav__item--parent.hover').removeClass('hover');
          $(wrap + ' .nav__item--parent.clicked').removeClass('clicked');
        }
        if($(e.target).parents(wrap + ' .link-list__item--parent').length == 0) {
          $(wrap + ' .link-list__item--parent.hover').removeClass('hover');
          $(wrap + ' .link-list__item--parent.clicked').removeClass('clicked');
        }
      });

      // Show dropdown in tablet
      $(document).on('click', wrap + ' .nav__item--parent:not(clicked) > a,' + wrap + ' .link-list__item--parent:not(clicked) > div > a', function(e) {
        if(curWinWidth >= 992 && curWinWidth <= 1199) {
          if(!$(this).parent().hasClass('clicked')) {
            e.preventDefault();
            $(this).parent().siblings('.clicked').removeClass('hover clicked');
            $(this).parent().addClass('clicked hover').trigger("mouseover");
          }
        }
      });
    }
    function resetNavWithData() {
      if(resetWithData) {
        alignDropdown(wrap);
      } else {
        resetDropdown(wrap);
        ins.applyCustomColorSwatches(wrap);
        alignDropdown(wrap);
        initHoverMenu(wrap);
        ins.initDealCountdown(wrap);
        resetWithData = true;
      }
    }
    function resetNavData() {
      if(ins.data.cacheWindowWidth >= 992) {
        if(loadedDropdown) {
          resetNavWithData();
        } else {
          ins.runLoadingDropdownNavInAjax(wrap, 'desktopNav', function() {
            loadedDropdown = true;
            resetNavWithData();
          });
        }
      }
    }
    if($(wrap).length > 0) {
      loadedDropdown = $(wrap).hasClass('loaded-dropdown');
      resetNavData();
      
      $(window).on(this.data.resizeEventName, function(e) {
        setTimeout(function() {
          resetNavData();
        }, 1000);
        curWinWidth = ins.data.cacheWindowWidth;
      });
    }
  },

  openMobileNav: function() {
    $('.mobile-nav').find('.link-list__item__link.active').each(function() {
      var link = $(this);
      link.parents('.link-list__item__dropdown').each(function() {
        var dropdown = $(this);
        dropdown.show();
        dropdown.parent('.link-list__item').addClass('open');
      });
    });
  },

  /* Mobile Navigation */
  initMobileNavigation: function() {
    var openClass = 'open', workingClass = this.selectors.workingClass;
    function showSubmenu(item) {
      if(item.hasClass(workingClass)) {
        return;
      }
      item.addClass(workingClass);
      item.children('.link-list__item__dropdown').slideDown(300, function() {
        item.removeClass(workingClass);
      });
    }
    function hideSubmenu(item) {
      if(item.hasClass(workingClass)) {
        return;
      }
      item.addClass(workingClass);
      item.children('.link-list__item__dropdown').slideUp(300, function() {
        item.removeClass(workingClass);
      });
    }
    $(document).on('click', '.mobile-nav__arrow', function(e) {
      e.preventDefault();
      var parent = $(this).parent();
      if(parent.hasClass(workingClass)) {
        return;
      }

      if(!parent.hasClass(openClass)) {
        parent.siblings('.' + openClass).each(function() {
          hideSubmenu($(this));
          $(this).removeClass(openClass);
        });
        parent.addClass(openClass);
        showSubmenu(parent);
      } else {
        parent.removeClass(openClass);
        hideSubmenu(parent);
      }
    });
    // this.openMobileNav();
  },

  showDropdown: function(ele) {
    this.runLoadAjaxSnippetWithEvent(ele);
    ele.addClass('active');
  },

  hideDropdown: function(ele){
    ele.removeClass('active');
  },

  initDropdown: function() {
    var dropdownSelector = this.selectors.dropdown,
      ins = this,
      overlaySelector = this.selectors.overlay;
    $(dropdownSelector.wrap).hover(function() {
      if(!ins.data.isTouchDevide) {
        ins.showDropdown($(this));
      }
    }, function(e) {
      if(!ins.data.isTouchDevide && !$(e.target).is(':focus')) {
        ins.hideDropdown($(this));
      }
    });
    $(document).on('click', dropdownSelector.trigger, function(e) {
      if(ins.data.isTouchDevide) {
        e.preventDefault();
        var parent = $(this).parents(dropdownSelector.wrap).first();
        if(parent.hasClass('active')) {
          ins.hideDropdown(parent);
        } else {
          var dataGroup = parent.attr('data-group');
          if(dataGroup != undefined) {
            $(dropdownSelector.wrap + '[data-group="' + dataGroup + '"].active').removeClass('active');
          }
          ins.showDropdown(parent);
        }
      }
    });
    $(document).on('click', 'body', function(e) {
      if($(e.target).parents(dropdownSelector.wrap).length == 0) {
        if($(dropdownSelector.wrap + '.active').length > 0) {
          $(dropdownSelector.wrap + '.active').removeClass('active');
        }
      }
    });
  },

  getInputFloatSelector: function() {
    var floatSelector = this.selectors.floatField;
    return floatSelector + ' input[type="text"],' 
      + floatSelector + ' input[type="password"],'
      + floatSelector + ' input[type="number"],'
      + floatSelector + ' input[type="email"],'
      + floatSelector + ' input[type="tel"]';
  },

  initFloatField: function() {
    var floatSelector = this.selectors.floatField, ins = this;
    var inputSelector = this.getInputFloatSelector();
    $(document).on('focus', inputSelector, function() {
      $(this).parents(floatSelector).addClass('active focus');
    });
    $(document).on('blur', inputSelector, function() {
      if($(this).val() == '') {
        $(this).parents(floatSelector).removeClass('active');
      }
      $(this).parents(floatSelector).removeClass('focus');
    });
    if($(this.selectors.floatFieldFirstFocus).length > 0) {
      $(this.selectors.floatFieldFirstFocus).parents(floatSelector).addClass('active focus');
    }
  },

  detectAutoFields: function() {
    var floatSelector = this.selectors.floatField;
    var inputSelector = this.getInputFloatSelector();
      $(inputSelector).each(function() {
        try {
          if($(this).val() != '' || $(this).is(":-webkit-autofill")) {
            $(this).parents(floatSelector).addClass('active');
          }
        } catch(e) {
          if($(this).val() != '') {
            $(this).parents(floatSelector).addClass('active');
          }
        }
    });
  },

  loadCssJsLib: function(libName, cb) {
    if(theme.loadedLibs.indexOf(libName) > -1) {
      if(cb) {
        cb();
      }
    } else {
      if(theme.libs[libName] != undefined) {
        var cssLibs = theme.libs[libName].css, jsLib = theme.libs[libName].js, ins = this;
        if(cssLibs != undefined) {
          $.each(cssLibs, function(index, fileUrl) {
            $('<link/>', {
               rel: 'stylesheet',
               type: 'text/css',
               href: 'https:' + fileUrl
            }).appendTo('head');
          });
        }
        if(jsLib != undefined) {
          $.getScript(
            'https:' + jsLib
          ).then(function() {
            theme.loadedLibs.push(libName);
            if(cb) {
              cb();
            }
          });
        } else {
          theme.loadedLibs.push(libName);
          if(cb) {
            cb();
          }
        }
      }
    }
  },

  /* Sidebar */
  initSidebar: function() {
    var ins = this;
    $(this.selectors.sidebar.box).removeClass('hide');
    $(document).on(this.options.sidebar.openEvent, this.selectors.sidebar.btn, function(e) {
      e.preventDefault();
      e.stopPropagation();
      var dataPlacement = $(this).attr('data-placement');
      var dataPlacementMobile = $(this).attr('data-placement-mobile');
      var direction = (ins.data.cacheWindowWidth >= 768) ? dataPlacement : (dataPlacementMobile == undefined ? dataPlacement : dataPlacementMobile);
      var block = $(this).attr('data-target');
      if(!$('body').hasClass(ins.selectors.sidebar.openClass)) {
        ins.showSidebar(block, direction, ins.options.sidebar.duration);
      }
    });
    this.initHideNavMobile(300);
  },
  initHideNavMobile: function(duration) {
    var ins = this;
    var triggerSelector = this.selectors.overlay + ',' + this.selectors.sidebar.closeBtnCanvas + ',' + this.selectors.sidebar.closeSidebarBtn;
    $(triggerSelector).on(ins.options.sidebar.closeEvent, function(e) {
      e.preventDefault();
      e.stopPropagation();
      if($('body').hasClass(ins.selectors.sidebar.openClass)) {
        ins.hideSidebar(duration, $(ins.selectors.overlay).attr('data-block'));
      }
    });
  },
  showSidebar : function(block, direction, duration) {
    var width = $(block).outerWidth();
    //$('.wrap-all').css({'overflow-x': 'inherit'});
    var dataShow = {}
    $(block).removeAttr('style');
    if(direction == 'left') {
      dataShow.left = 0;
      $(block).css({left: '-100%'});
      $(block).css({right: 'auto'});
    } else if(direction == 'right') {
      dataShow.right = 0;
      $(block).css({right: '-100%'});
      $(block).css({left: 'auto'});
    } else {
      dataShow = 'slideDown';
    }
    this.runLoadAjaxSnippetWithEvent($(block));
    var overlaySelector = this.selectors.overlay;
    $(overlaySelector).attr('data-block', block);

    var ins = this;
    if(dataShow == 'slideDown') {
      var ownCanvasOpenClass = ins.selectors.sidebar.openClass + '--' + $(block).attr('id');
      $('body').addClass(ins.selectors.sidebar.openClass + ' ' + ownCanvasOpenClass);
      $(block).slideDown(duration, function() {
        $(overlaySelector).addClass(direction);
      });
    } else {
      var ownCanvasOpenClass = ins.selectors.sidebar.openClass + '--' + $(block).attr('id');
      $('body').addClass(ins.selectors.sidebar.openClass + ' ' + ownCanvasOpenClass);
      $(block).animate(dataShow,{
        duration: duration,
        complete: function() {
          $(overlaySelector).addClass(direction);
        }
      });
    }
    $(overlaySelector).fadeIn(ins.options.sidebar.duration);
  },
  hideSidebar: function(duration, block) {
    var overlaySelector = this.selectors.overlay;
    var direction = $(overlaySelector).hasClass('left') ? 'left' : ($(overlaySelector).hasClass('up') ? 'up' : 'right');  
    var width = $(block).outerWidth();
    var dataHide = {};//, dataCloseBtn = {opacity: 0, top: '-100px'};
    if(direction == 'left') {
      dataHide.left = '-100%';
    } else if(direction == 'right') {
      dataHide.right = '-100%';
    } else {
      dataHide = 'slideUp';
    }
    var ownCanvasOpenClass = this.selectors.sidebar.openClass + '--' + $(block).attr('id');
    $('body').removeClass(this.selectors.sidebar.openClass + ' ' + ownCanvasOpenClass);
    if(dataHide == 'slideUp') {
      $(block).slideUp(duration, function() {
        $(overlaySelector).removeClass(direction);
        $(block).removeAttr('style');
      });
    } else {
      $(block).animate(dataHide, {
        duration: duration,
        complete: function() {
          $(overlaySelector).removeClass(direction);
          $(block).removeAttr('style');
        }
      });
    }
    $(overlaySelector).fadeOut(duration);
  },

  initSlickSlider: function(ele, useEffect) {
    var dataSlider = {};
    $.extend(true, dataSlider, this.options.slider);
    if(ele.attr('data-slider')) {
      try {
        dataSlider = $.extend(true, dataSlider, JSON.parse(ele.attr('data-slider')));
      } catch(e) {
        ele.addClass('failed');
        dataSlider = this.options.slider;
      }
    }
    if(useEffect) {
      ele.off('beforeChange');
      ele.off('afterSlideChange');
      ele.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        var $slider = slick.$slider;
        var $currentSlide = $slider.find('.slick-current').addClass('animate-out');
      });
      ele.on('afterChange', function(event, slick, currentSlide) {
        var $slider = slick.$slider;
        var $allSlides = $slider.find('.slick-slide').removeClass('animate-out');
      });
    }
    ele.slick(dataSlider);
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(isSafari && ele.hasClass('need-refresh')) {
      ele.slick('refresh');
    }

  },
  initSliderEle: function(ele, useEffect) {
    if(this.data.cacheWindowWidth >= this.options.windowScreen.desktop || !ele.hasClass(this.selectors.slider.useTouchMobile)) {
      var parent = ele.parent();
      parent.height(parent.height());
      parent.css({'overflow': 'hidden'});
      this.preInitSlider(ele);
      this.initSlickSlider(ele, useEffect);
      setTimeout(function() {
        ele.parent().removeAttr('style');
      }, 500);
    } else {
      this.preInitSlider(ele);
    }
  },
  preInitSlider: function(wrap, includeHideClass) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var classes = this.selectors.slider.overFloatClasses;
    if(includeHideClass) {
      classes += ' hide';
    }
    wrap.find('.' + classes.replace(/ /g,',.')).removeClass(classes);
  },
  triggerResizeSlider: function(ele) {
    var ins = this;
    if(ele.hasClass(ins.selectors.slider.useTouchMobile)) {
      $(window).on(ins.data.resizeEventName, function() {
        if(ins.data.cacheWindowWidth >= ins.options.windowScreen.desktop) {
          if(!ele.hasClass('slick-initialized')) {
            ins.initSlickSlider(ele);
          }
        } else {
          if(ele.hasClass('slick-initialized') && ele.hasClass(ins.selectors.slider.useTouchMobile)) {
            setTimeout(function() {
              ele.slick('unslick');
            }, 300);
          }
        }
      });
    }
  },

  getSliderSelector: function() {
    return this.selectors.slider.default + ':not(' + this.selectors.waitingData + ')';
  },

  initSlider: function(wrap, wait, useEffect) {
    var ins = this;
    var sliderSelector = this.getSliderSelector(), touchMobileWrap = 'touch-mobile';
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var sliders = wrap.find(sliderSelector);
    if(!sliders.length) {
      return;
    }

    if(this.data.cacheWindowWidth >= this.options.windowScreen.desktop || !wrap.hasClass(touchMobileWrap)) {
      if(wait) {
        var suffixEvent = 'init_slider_' + this.data.sliderIndex;
        this.data.sliderIndex++;
        this.initScrollingWindowTriggerOnce(wrap, suffixEvent,  -170, function() {
          sliders.each(function() {
            var ele = $(this);
            ins.initSliderEle(ele, useEffect);
            ins.triggerResizeSlider(ele);
          });
        });
      } else {
        sliders.each(function() {
          var ele = $(this);
          ins.initSliderEle(ele, useEffect);
          ins.triggerResizeSlider(ele);
        });
      }
    } else {
      ins.preInitSlider(wrap);
      sliders.each(function() {
        var ele = $(this);
        ins.triggerResizeSlider(ele);
      });
    }
  },

  destroySlider: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var sliders = wrap.find(this.getSliderSelector());
    if(!sliders.length) {
      return;
    }
    sliders.each(function() {
      var ele = $(this);
      if(ele.hasClass('slick-initialized')) {
        ele.slick('unslick');
      }
    });
  },

  refreshSliderInTabs: function() {
    var ins = this;
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var windowWidth = ins.data.cacheWindowWidth;
      var tabContent = $(e.target).attr('href');
      var sliderSelector = ins.selectors.slider.default + ':not(' + ins.selectors.waitingData + ')';
      if($(tabContent).find(sliderSelector).length > 0) {
        $(tabContent).find(sliderSelector).each(function() {
          var ele = $(this);
          if(windowWidth >= ins.options.windowScreen.desktop) {
            if(ele.hasClass('slick-initialized')) {
              ele.slick('setPosition');
            }
          } else {
            if(!ele.hasClass(ins.selectors.slider.useTouchMobile) && ele.hasClass('slick-initialized')) {
              ele.slick('setPosition');
            }
          }
        });
      }
    });
  },

  /* Instagram section */
  initInstagram : function (instagramEle) {
    var token = instagramEle.attr('data-token');
    var ins = this, workingClass = this.selectors.workingClass;
    if(token != undefined && token != '') {
      var numPhotos = instagramEle.attr('data-num-photos');
      this.initScrollingWindowTriggerOnce(instagramEle, 'instagram', -300, function() {
        if(instagramEle.hasClass(workingClass)) {
          return;
        }
        instagramEle.addClass(workingClass);
        $.ajax({
          url: 'https://api.instagram.com/v1/users/self/media/recent', // or /users/self/media/recent for Sandbox
          dataType: 'jsonp',
          type: 'GET',
          data: {access_token: token, count: numPhotos},
          success: function(data){
            instagramEle.removeClass(workingClass);
            var sliderClass = ins.selectors.slider.default.replace('.','');
            var useTouchClass = ins.selectors.slider.useTouchMobile;
            var index = 1;
            instagramEle.html('');
            $.each(data.data, function(index, value){
              var titleTag = (value.caption != null && value.caption.text != undefined) ? value.caption.text : 'instagram';
              var html = '<div class="item grid__item effect-hover no-gutter' + (index > 4 ? ' ov-slider-tb' : '') + '">';
              html += '<div class="item__top por">' +
              '<a ref="noreferrer" title="' + titleTag + '" href="' + value.link + '" class="item__image loading" target="_blank">' +
              '<img data-src="' + value.images.standard_resolution.url + '" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload img-responsive" alt="' + titleTag + '"/>' +
              '</a><div class="hidden-xs hidden-sm text-center effect-hover__content effect-hover__bkg effect-hover__content--middle w100 h100 flex flex-align-hoz flex-align-ver"><a ref="noreferrer" title="' + titleTag + '" href="' + value.link + '" target="_blank" class="button button--icon button--instagram"><i class="wicon-instagram"></i></a></div></div>' +
              '</div>';
              index++;
              instagramEle.append(html);
            });
            instagramEle.addClass(sliderClass).addClass(useTouchClass);
            ins.initSlider('#' + instagramEle.attr('data-section-id'), false, false);
          },
          error: function(data){
            instagramEle.removeClass(workingClass);
          }
        });
      });
    }
  },

  submitCartForm: function(form, btn, cb, cbError) {
    var ins = this;
    if(form.valid()) {
      /*var progressWrap = btn.siblings('.progress');
      var progressBar = progressWrap.find('.progress-bar');
      if(progressWrap) {
        progressWrap.removeClass('hide');
      }*/
      form.ajaxSubmit({
        url: theme.cartAddUrl,
        type: 'post',
        dataType: 'json',
        error: function(xhr, textStatus, errorThrown) {
          if(xhr.responseJSON.description != undefined && xhr.responseJSON.description != null) {
            ins.showNotify(xhr.responseJSON.description, ins.options.notifyClassTypes.danger);
          } else if (xhr.responseJSON.message != undefined) {
            ins.showNotify(xhr.responseJSON.message, ins.options.notifyClassTypes.danger);
            //showNotifyMsg(xhr.responseJSON.message, 'error');
          }
          ins.endLoadingBtn(btn);
          btn.removeClass(ins.selectors.workingClass);
          //showNotifyMsg(xhr.responseJSON.description, 'error');
          /*if(progressWrap) {
            progressBar.width('0%').text('0%');
            progressWrap.addClass('hide');
          }*/
        },
        uploadProgress: function(event, position, total, percentComplete) {
          /*if(progressWrap) {
            progressBar.width(percentComplete + '%').text(percentComplete + '%');  
          }*/
        },
        success: function(data, textStatus, jqXHR, $form) {
          /*if(progressWrap) {
            progressBar.width('0%').text('0%');
            progressWrap.addClass('hide');
          }*/
          cb(data);
        }
      });
    } else {
      cbError();
    }
  },

  updateFaviconBadge: function(number) {
    if(!theme.favicon.enable || this.data.cacheWindowWidth < 768) {
      return false;
    }
    if(this.data.favicon == null) {
      this.data.favicon = new Favico({
        animation:'slide',
        position: theme.favicon.pos,
        bgColor : theme.favicon.bkgColor,
        textColor : theme.favicon.textColor,
      });
    }
    number = parseInt(number);
    if(number > 0) {
      this.data.favicon.badge(number);
    } else {
      this.data.favicon.reset();
    }
  },

  updateHeaderCartHtml: function(ignoreCartAction) {
    if((theme.cartAction == 'redirect' || theme.cartAction == 'redirect_checkout' ) && ignoreCartAction == false) {
      return;
    }
    var loadedHeaderCart = this.data.loadedSnippets.indexOf('header-cart-content');
    this.data.loadedSnippets.push('header-cart-content');
    var cartSelector = this.selectors.cart, ins = this;
    new Promise(function(resolve, reject) {
      ins.callAjax(theme.rootUrl, 'GET', {view: 'header_cart'}, null, function(response) {
        var htmlHeaderCart, headerCartSelector;
        if(loadedHeaderCart == -1) {
          htmlHeaderCart = $(cartSelector.header, response)[0].outerHTML;
          headerCartSelector = '[data-snippet="header-cart-content"]';
        } else {
          htmlHeaderCart = $(cartSelector.header, response).html();
          headerCartSelector = cartSelector.header;
        }
        var snippetAjaxClass = ins.selectors.ajaxSnippet.replace('.','');
        $(headerCartSelector).each(function() {
          $(this).html(ins.processSameId(htmlHeaderCart));
          if(loadedHeaderCart == -1) {
            $(this).removeClass(snippetAjaxClass).removeAttr('data-snippet')
          }
        });
        var number = $(cartSelector.number, response).html();
        var total = $(cartSelector.total, response).html();
        $(cartSelector.number).html(number);
        $(cartSelector.total).html(total);
        ins.updateFaviconBadge(number);
        resolve(cartSelector.header);  
      });
    }).then(function(wrapper) {
      if(wrapper != undefined) {
        ins.convertCurrencySilence(wrapper + ' span.money');
        $(document).trigger('ajaxCart.afterUpdateHeaderCartHtml');
      }
      ins.convertCurrencySilence(cartSelector.total + ' span.money');
      ins.resetCartTerms();
      $(document).trigger('ajaxCart.afterCartLoad');
      // Not run below code on cart page
      if (window.Shopify && Shopify.StorefrontExpressButtons && !$('body').hasClass('template-cart')) {
        Shopify.StorefrontExpressButtons.initialize();
      }
    }, function(err) {
      // console.log(err);
    });
  },

  generateCartMessage: function(productTitle) {
    return theme.strings.addCartMsg.replace(this.options.productNameHolder, '<strong>' + productTitle + '</strong>');
  },

  updateCartMessage: function(productTitle) {
    $(this.selectors.cartMessage).removeClass('hide').html(this.generateCartMessage(productTitle));
  },

  showPopupCart: function(cartResponse, cb) {
    var ins = this;
    this.loadSnippetAjax('popup-cart', function() {
      ins.loadUpsellInPopup(cartResponse.handle, cartResponse.title);
      ins.updateCartMessage(cartResponse.title);
      var selector = ins.selectors.cartModal;
      // wrap: '#cartModal',
      //   productImage: '.cart-response__image',
      //   productName: '.cart-response__name',
      //   variantTitle: '.cart-response__meta-variant-title',
      //   variantProp: '.cart-response__meta-propeties',
      //   price: '.cart-response__price',
      //   qty: '.cart-response__qty'
      $(selector.productImage).attr('href', cartResponse.url)
        .attr('title', cartResponse.title)
        .find('img')
        .attr('src', ins.resizeImage(cartResponse.image, '360x'))
        .attr('alt', cartResponse.title);
      
      $(selector.productName + ' > a').attr('title', cartResponse.title)
        .attr('href', cartResponse.url)
        .html(cartResponse.product_title);
      
      if(cartResponse.variant_options.length > 0) {
        $(selector.variantTitle).removeClass('hide').html(cartResponse.variant_title);      
      } else {
        $(selector.variantTitle).addClass('hide');
      }

      if(cartResponse.properties) {
        var prop = $(selector.variantProp);
        prop.removeClass('hide');
        prop.html('');
        $.each(cartResponse.properties, function( index, p ){
          prop.append(index + ':');
          if(p.indexOf('/uploads/') > 0) {
            prop.append(p.split("/").pop());
          } else {
            prop.append(p);
          }
        });
      } else {
        $(selector.variantProp).addClass('hide');
      }

      $(selector.qty).html(cartResponse.quantity); 
      $(selector.price).html(ins.getPriceHtml(cartResponse.price)); 
      $(selector.linePrice).html(ins.getPriceHtml(cartResponse.line_price));
      ins.resetCartTerms();
      if(theme.enableFreeShipping) {
        ins.callAjax(theme.cartJsonUrl, 'get', null, 'json', function(cartObj) {
          var freeShippingPercent;
          if(cartObj.total_price >= theme.freeShippingGoal) {
            $(selector.freeShippingText).html(theme.freeShippingGoalText);
            freeShippingPercent = 100;
          } else {
            var remain_amount = theme.freeShippingGoal - cartObj.total_price;
            var remain_amount_html = '<strong>' + ins.getPriceHtml(remain_amount) + '</strong>';
            $(selector.freeShippingText).html(theme.freeShippingProgressText.replace('[remain_amount]',remain_amount_html));
            freeShippingPercent = Math.round((cartObj.total_price*100)/theme.freeShippingGoal);
          }
          $(selector.freeShippingProgressBar).attr('aria-valuenow',freeShippingPercent).css('width',freeShippingPercent + '%');
          $(selector.freeShippingProgressBar + ' span').text(freeShippingPercent + '%');
          BtCurrency.convertSilence(shopCurrency, BtCurrency.currentCurrency, selector.wrap + ' span.money');
          ins.showPopup(selector.wrap);
          cb();
        });
      } else {
        BtCurrency.convertSilence(shopCurrency, BtCurrency.currentCurrency, selector.wrap + ' span.money');
        ins.showPopup(selector.wrap);
        cb();
      }
    });
  },

  enableCartTerms: function() {
    $(this.selectors.cartTerms.wrap).show();
    $(this.selectors.cartTerms.checkoutBtn).attr('disabled','disabled');
    $(this.selectors.cartTerms.paymentBtn).addClass('disabled');
  },

  disableCartTerms: function() {
    $(this.selectors.cartTerms.wrap).hide();
    $(this.selectors.cartTerms.checkoutBtn).removeAttr('disabled');
    $(this.selectors.cartTerms.paymentBtn).removeClass('disabled');
  },

  resetCartTerms: function() {
    if(this.agreeCartTerms()) {
      this.disableCartTerms();
    } else {
      this.enableCartTerms();
    }
  },

  initAgreeCartTems: function(){
    var ins = this, cookieName = this.options.cartTerms.cookieName;
    this.resetCartTerms();
    $(document).on('change', this.selectors.cartTerms.checkbox, function(e) {
      e.preventDefault();
      if($(this).is(':checked')) {
        ins.setCookie(cookieName, 1);
        if($(this).hasClass('redirect')) {
          window.location.href = '/checkout';
        } else {
          $(ins.selectors.cartTerms.checkbox).not(this).prop('checked', true);
          $(ins.selectors.cartTerms.checkoutBtn).removeAttr('disabled');
          $(ins.selectors.cartTerms.paymentBtn).removeClass('disabled');
        }
      } else {
        ins.setCookie(cookieName, null);
        $(ins.selectors.cartTerms.checkbox).not(this).prop('checked', false);
        $(ins.selectors.cartTerms.checkoutBtn).attr('disabled','disabled');
        $(ins.selectors.cartTerms.paymentBtn).addClass('disabled');
      }
    });
  },

  agreeCartTerms: function() {
    if(!theme.cartTerms) {
      return true;
    }
    var cookieName = this.options.cartTerms.cookieName;
    var cookie = this.getCookie(cookieName);
    if(cookie == undefined || cookie == null || cookie == 'null') {
      return false;
    }
    return true;
  },

  runCartAction: function(cartResponse, cb) {
    if(theme.cartAction == 'redirect') {
      window.location.href = theme.cartUrl;
    } else if (theme.cartAction == 'redirect_checkout') {
      if(this.agreeCartTerms()) {
        window.location.href = '/checkout';
      } else {
        this.updateHeaderCartHtml(true);
        var ins = this;
        this.loadSnippetAjax('popup-terms', function() {
          if($(ins.selectors.cartTerms.popup).length > 0) {
            ins.showPopup(ins.selectors.cartTerms.popup);
            cb();
          }
        });
      }
    } else if(theme.cartAction == 'popup') {
      this.updateHeaderCartHtml(false);
      this.showPopupCart(cartResponse, cb);
    } else if(theme.cartAction == 'open_cart') {
      this.updateHeaderCartHtml(false);
      $('.cart-sidebar-trigger').trigger('click');
      cb();
    } else if(theme.cartAction == 'notify_msg') {
      this.updateHeaderCartHtml(false);
      this.showNotify(this.generateCartMessage(cartResponse.title), this.options.notifyClassTypes.success);
      cb();
    }
  },

  initCartEvent: function () {
    var ins = this, cartSelector = this.selectors.cart;
    $(document).on('click', cartSelector.addBtn, function(e) {
      e.preventDefault();
      var btn = $(this);
      if(btn.hasClass(ins.selectors.workingClass)) {
        return;
      }
      // Add product to cart
      var form = $(this).parents('form');

      btn.addClass(ins.selectors.workingClass);
      ins.startLoadingBtn(btn);
      ins.submitCartForm(form, btn, function(response) {
        if(typeof(fbq) != 'undefined') {
          fbq('track', 'AddToCart', {
            content_name: response.title,
            content_ids: [response.product_id],
            content_type: 'product_group',
            content_category: response.product_type,
            value: BtCurrency.convertNumber(response.final_line_price, shopCurrency, BtCurrency.currentCurrency),
            // value: BtCurrency.convertNumber(response.price, shopCurrency, BtCurrency.currentCurrency),
            currency: BtCurrency.currentCurrency  
            // currency: enableCurrencyConverter ? BtCurrency.currentCurrency : shopCurrency  
          });
        }
        ins.runCartAction(response, function() {
          ins.endLoadingBtn(btn);
          if((theme.cartAction == 'open_cart' || theme.cartAction == 'notify_msg') && btn.parents('.modal').length > 0) {
            ins.hidePopup('#' + btn.parents('.modal').first().attr('id'));
          }
        });
        btn.removeClass(ins.selectors.workingClass);
      }, function() {
        ins.endLoadingBtn(btn);
        btn.removeClass(ins.selectors.workingClass);
      });
    });

    $(document).on('click', cartSelector.updateBtn, function(e) {
      e.preventDefault();
      if($(this).hasClass(ins.selectors.workingClass)) {
        return;
      }
      $(this).addClass(ins.selectors.workingClass);
      if($(this).attr('data-toggle') == 'tooltip') {
        $(this).tooltip('hide');
      }
      var line = $(this).attr('data-index');
      var value = $(this).attr('data-value');
      var i = $(this).find('i');
      i.addClass(ins.selectors.loadingAjaxClass);
      if(isNaN(value)) {
        if($(value).length == 0) {
          $(this).find('i').removeClass(ins.selectors.loadingAjaxClass);
          $(this).removeClass(ins.selectors.workingClass);
          return;
        }
        // value is selector to an input
        value = $(value).val();
      }
      ins.requestUpdateCart(line, value, function() {
        ins.updateHeaderCartHtml(true);
      }, function() {
        i.removeClass(ins.selectors.loadingAjaxClass);
      });
    });

    $(document).on('change', cartSelector.qtyInput, function() {
      var line = $(this).attr('data-line');
      ins.showLoadingFull();
      ins.requestUpdateCart(line, $(this).val(), function() {
        ins.updateHeaderCartHtml(true);
        ins.hideLoadingFull();
      }, function() {
        ins.hideLoadingFull();
      });
    });
  },

  hideSearchFull: function() {
    $(this.selectors.searchFull.wrap).fadeOut();
    $('body').removeClass('open-search-suggest');
  },

  renderSearchFullContent: function(html) {
    var selector = this.selectors.searchFull;
    this.data.searchSuggestionCurrentAjaxRequest = null;
    $(selector.loading).removeClass(this.selectors.loadingAjaxClass);
    $(selector.content + '__' + this.data.searchSuggestionCurrentType).addClass(selector.loadedClass);
    var resultWrap = $(selector.content + '__' + this.data.searchSuggestionCurrentType + ' ' + selector.result);
    resultWrap.html(html);
    if(this.data.searchSuggestionCurrentType == 'product') {
      this.convertCurrencySilence(resultWrap.find('span.money'));
      this.initDealCountdown(resultWrap);
      this.applyCustomColorSwatches(resultWrap);
      this.reLoadReview(resultWrap);
      this.popularAddedWishlistItems(resultWrap);
    }
  },

  initSearchSuggestion: function () {
    var selector = this.selectors.searchFull, workingClass = 'working';
    var ins = this;
    $(document).on('click', selector.trigger, function(e) {
      e.preventDefault();
      if($(this).hasClass(workingClass)) {
        return;
      }
      var btn = $(this);
      btn.addClass(workingClass);
      if(ins.data.loadedSnippets.indexOf('search-full') == -1) {
        ins.showLoadingFull();
      }
      ins.loadSnippetAjax('search-full', function() {
        ins.hideLoadingFull();
        btn.removeClass(workingClass);
        $(selector.wrap).fadeIn();
        $(selector.input).focus();
        $('body').addClass('open-search-suggest');
      });
    });
    $(document).on('click', selector.close, function(e) {
      e.preventDefault();
      ins.hideSearchFull();
    });
    if(!theme.searchSuggest.enable) {
      return;
    }
    var loadedClass = 'loaded';
    var needResetLoaded = false;
    var dataSearch = {options:{prefix:'last'}};
    $(document).on('keyup change', selector.input, $.throttle(500, function(evt) {
      var input = $(this);
      var term = input.val(); 
      if (term.length >= 3 && term != input.attr('data-term')) {
        if (ins.data.searchSuggestionCurrentAjaxRequest != null) {
          ins.data.searchSuggestionCurrentAjaxRequest.abort();
        }
        var searchURL = theme.searchUrl + '?view=suggest&q=' + term + '&type=' + ins.data.searchSuggestionCurrentType;
        $(selector.loading).addClass(ins.selectors.loadingAjaxClass);
        input.attr('data-term', term);
        needResetLoaded = true;
        ins.data.searchSuggestionCurrentAjaxRequest = ins.callAjax(searchURL, 'get', dataSearch, null, function(html) {
          $(selector.tabList).removeClass('hide');
          $(selector.content).removeClass('hide');
          $(selector.tabContent + ':not(.active)').removeClass(loadedClass);
          needResetLoaded = false;
          ins.renderSearchFullContent(html);
        });
      }
    }));
    $(document).on('shown.bs.tab', selector.tabTitle, function (e) {
      var target = $(e.target);
      var tabContent = $(target.attr('href'));
      if(needResetLoaded) {
        $(selector.tabContent + ':not(.active)').removeClass(loadedClass);
        needResetLoaded = false;
      }
      ins.data.searchSuggestionCurrentType = target.attr('data-type');
      if(!tabContent.hasClass(selector.loadedClass)) {
        if (ins.data.searchSuggestionCurrentAjaxRequest != null) { 
          ins.data.searchSuggestionCurrentAjaxRequest.abort();
          
        };
        var url = theme.searchUrl + '?view=suggest&q=' + $(selector.input).val() + '&type=' + ins.data.searchSuggestionCurrentType;
        ins.data.searchSuggestionCurrentAjaxRequest = ins.callAjax(url, 'get', dataSearch, null, function(html) {
          ins.renderSearchFullContent(html);
        });
      }
    });
    this.initSearchSuggestionInline();
  },

  initSearchSuggestionInline: function() {
    if(!theme.searchSuggest.enable) {
      return;
    }
    var currentAjaxRequest = null, ins = this;
    $(document).on('keyup change', 'form[action="' + theme.searchUrl + '"] input.search-suggest-inline', function() {
      // What's the search term?
      var term = $(this).val();
      // What's the search form?
      var form = $(this).closest('form');
      // What's the search URL?
      var searchURL = theme.searchUrl + '?type=product&options[prefix]=last&q=' + term;
      // What's the search results list?
      var resultsWrap = form.siblings('.search-suggest');
      var resultsTotal = resultsWrap.find('.search-suggest__total');
      var resultsList = resultsWrap.find('.search-suggest__list');
      var icon = $(this).siblings('button').find('i');
      // If that's a new term and it contains at least 3 characters.
      if (term.length >= 3 && term != $(this).attr('data-old-term')) {
        // Saving old query.
        $(this).attr('data-old-term', term);
        // Killing any Ajax request that's currently being processed.
        if (currentAjaxRequest != null) currentAjaxRequest.abort();
        icon.addClass(ins.selectors.loadingAjaxClass);
        // Pulling results.
        currentAjaxRequest = $.getJSON(searchURL + '&view=json', function(data) {
          icon.removeClass(ins.selectors.loadingAjaxClass);
          // Reset results.
          resultsList.empty();
          resultsTotal.empty();
          // If we have no results.
          if(data.results_count == 0) {
            // resultsList.html('<li><span class="title">No results.</span></li>');
            // resultsList.fadeIn(200);
            resultsWrap.hide();
          } else {
            resultsTotal.html(data.results_total_text);
            // If we have results.
            $.each(data.results, function(index, item) {
              var link = $('<a class="flex"></a>').attr('href', item.url);
              link.append('<div class="search-suggest__thumbnail"><img src="' + item.thumbnail + '" class="img-responsive"/></div>');
              if(item.title || item.price) {
                var div = $('<div class="search-suggest__info"></div>');
                if(item.title) {
                  div.append('<span class="search-suggest__title db">' + item.title + '</span>');
                }
                if(item.price) {
                  div.append('<span class="product-price__price">' + ins.getPriceHtml(item.price) + '</span>');
                }
                link.append(div);
              }
              link.wrap('<li class="gutter-ele-top"></li>');
              resultsList.append(link.parent());
            });
            ins.convertCurrencySilence('.search-suggest span.money');
            // The Ajax request will return at the most 10 results.
            // If there are more than 10, let's link to the search results page.
            if(data.results_count > theme.searchSuggest.limit) {
              resultsList.append('<li class="gutter-ele-top"><span class="title"><a href="' + searchURL + '">' + data.results_see_all_text + '</a></span></li>');
            }
            resultsWrap.fadeIn(200);
          }        
        });
      }
    });

    // Clicking outside makes the results disappear.
    $('body').bind('click', function(){
      $('.search-suggest').hide();
    });
  },

  loadUpsell: function(wrap) {
    if(typeof wrap == 'undefined' || wrap.length == 0) {
      return;
    }
    var ins = this, selector = this.selectors.upsell;
    var productHandle = wrap.attr('data-handle');
    var list = wrap.find(selector.list);
    if(list.hasClass('slick-initialized')) {
      list.slick('unslick');
    }
    list.html('');
    list.siblings(this.selectors.loadingNotFull).show();
    wrap.show();
    var limit = upsellRandom ? 100 : parseInt(wrap.attr('data-limit'));
    this.callAjax(theme.rootUrl + 'products/' + productHandle, 'GET', {view:'upsell_tags'}, null, function(responseTags) {
      new Promise(function(resolve, reject) {
        var upsellTags = $.parseJSON(responseTags.replace('<!-- BEGIN template -->','').replace('<!-- product.upsell_tags -->','').replace('<!-- END template -->','')), upsellList = [], upsellIds = [], index = 0;
        var tagsLength = upsellTags.length;
        if(tagsLength == 0) {
          resolve([]);
        }
        var view = wrap.attr('data-slider') ? 'upsell_slider' : 'upsell';
        $(upsellTags).each(function(indexTag, tag) {
          ins.callAjax(theme.collectionAllUrl + '/' + tag, 'GET', {view:view}, null, function(responseUpsell) {
            $('.product-item', responseUpsell).each(function() {
              if(upsellIds.indexOf($(this).attr('data-id')) < 0 && $(this).attr('data-handle') != productHandle && index < limit) {
                upsellList.push($(this).html());
                upsellIds.push($(this).attr('data-id'));
                index++;
              }
            });
            
            if(indexTag == tagsLength - 1) {
              if(upsellRandom) {
                upsellList = ins.shuffleArray(upsellList);
              }
              resolve(upsellList);
            }
          });  
        });
      }).then(function(upsellList) {
        list.siblings(ins.selectors.loadingNotFull).hide();
        if(upsellList.length > 0) {
          var index = 0;
          $(upsellList).each(function(i, value) {
            if(index < limit) {
              list.append(value);  
            }
            index++;
          });
          ins.convertCurrencySilence(list.find('span.money'));
          ins.applyCustomColorSwatches(list);
          ins.initSlider(wrap, false, false);
          ins.popularAddedWishlistItems(wrap);
        } else {
          wrap.hide();
        }
      }, function(e) {
        // console.log(e);
      });
    });
  },

  loadUpsellInPopup: function(productHandle, productTitle) {
    var ins = this;
    var upsellEle = $(this.selectors.cartModal.wrap + ' ' + this.selectors.upsell.wrap);
    if(typeof upsellEle == 'undefined' || upsellEle.length == 0) {
      return;
    }
    upsellEle.attr('data-handle', productHandle);
    upsellEle.find('.upsell-title').html(theme.strings.upsellTitle.replace(this.options.productNameHolder, '<strong>' + productTitle + '</strong>'));
    this.loadUpsell(upsellEle);
  },

  loadRecentViewedProductsSingle: function(point) {
    if(typeof point == 'undefined' || point.length == 0) {
      return;
    }
    var ins = this;
    var wrap = point.find('.rencent-view-ele');
    var list = wrap.find('.recent-view-list');
    var productHandle = wrap.attr('data-handle');
    var limit = parseInt(wrap.attr('data-limit'));
    var handleItems = this.getCookieItemsValue(true, this.options.recentView.cookieName);
    var index = handleItems.indexOf(productHandle);
    if(index > -1) {
      handleItems.splice(index, 1);
    }
    if(handleItems.length > 0) {
      handleItems.reverse();
      var q = handleItems.join(',');
      this.callAjax(theme.searchUrl, 'GET', {view:'recent_view_slider', q: q}, null, function(html) {
        if(list.hasClass('slick-initialized')) {
          list.slick('unslick');
        }
        list.html(html);
        if(list.find('.grid__item').length > 0) {
          ins.convertCurrencySilence(list.find('span.money'));
          ins.applyCustomColorSwatches(list);
          wrap.fadeIn(300, function() {
            ins.initSlider(wrap, false, false);
            ins.popularAddedWishlistItems(wrap);
          });
        }
      });
    }
  },

  addRecommendationUrls: function(ele, urls) {
    var qvBtn = this.selectors.qvBtn;
    var hrefSelector = 'a.item__image,a.item__name,' + qvBtn;
    ele.find('.item').each(function() {
      var handle = $(this).attr('data-product-handle');
      if(urls[handle]) {
        $(this).find(hrefSelector).attr('href', urls[handle]);
        $(this).find('.effect-hover__bkg').attr('onclick', "window.location.href = '" + urls[handle] + "';");
      }
    })
  },

  sendRecommendationTrekkieEvent: function(grid) {
    if (
      !window.ShopifyAnalytics ||
      !window.ShopifyAnalytics.lib ||
      !window.ShopifyAnalytics.lib.track
    ) {
      return;
    }

    var numberOfRecommendationsDisplayed = grid.find(
      '.grid__item'
    ).length;

    window.ShopifyAnalytics.lib.track('Product Recommendations Displayed', {
      theme: Shopify.theme.name,
      didPageJumpOccur: true,
      numberOfRecommendationsDisplayed: numberOfRecommendationsDisplayed
    });
  },

  loadRecommendationProduct: function(ele) {
    var ins = this;
    if(ele.length > 0) {
      this.initScrollingWindowTriggerOnce(ele, 'recommendation', -170, function() {
        var productId = ele.attr('data-product-id');
        var limit = ele.attr('data-limit');
        var view = ele.data('view');
        if(!ele.hasClass('loaded')) {
          ins.callAjax(theme.recommendationUrl + '.json', 'GET', {product_id: productId, limit: limit}, 'json', function(response) {
            if(response.products.length > 0) {
              var handles = [], urls = {};
              $.each(response.products, function(index, product) {
                handles.push(product.handle);
                urls[product.handle] = product.url;
              });
              var handleQuery = handles.join(',');
              ins.callAjax(theme.searchUrl, 'GET', {view: view, 'q': handleQuery}, null, function(html) {
                var grid;
                if(view == 'recommendation') {
                  grid = ele.children('.cross-sell-list');
                  grid.html(html);
                  grid.addClass(ins.selectors.slider.useTouchMobile + ' ' + ins.selectors.slider.default.replace('.',''));
                  ins.initSlider(ele, false, false);
                  ins.initDealCountdown(grid);
                } else {
                  grid = ele.parents('.freb__inner').first();
                  grid.append(html);
                }
                ins.addRecommendationUrls(grid, urls);
                ins.sendRecommendationTrekkieEvent(grid); 
                ins.convertCurrencySilence(grid.find('span.money'));
                ins.applyCustomColorSwatches(grid);
                ins.reLoadReview(grid);
                BTFreBought.init();
                ele.parent().find('.loading-not-full').remove(); 
              });
            } else {
              ele.hide();
            }
          }, function() {
            ele.hide();
          })
        } else {
          var grid;
          if(view == 'recommendation') {
            grid = ele.children('.cross-sell-list');
            grid.addClass(ins.selectors.slider.useTouchMobile + ' ' + ins.selectors.slider.default.replace('.',''));
            ins.initSlider(ele, false, false);
            ins.initDealCountdown(grid);
            ins.popularAddedWishlistItems(grid);
          } else {
            grid = ele.parents('.freb__inner').first();
            BTFreBought.init(); 
          }
          ins.applyCustomColorSwatches(grid);
        }
      });
    }
  },

  gotoSliderIndex: function(slider, index) {
    slider.slick('slickGoTo', index);
  },

  initProductThumbsSlider: function(wrap) {
    var ins = this, prefixSelector = '', options = this.options.productGroup, selector = this.selectors.productGroup;
    if(wrap) {
      prefixSelector = wrap + ' ';
    }
    if($(prefixSelector + selector.thumbsSlider).length > 0) {
      $(prefixSelector + selector.thumbsSlider).each(function() {
        var optionsSlider = {};
        $.extend(true, optionsSlider, options.smallSlider);
        if($(this).hasClass(selector.thumbsSliderVerticalClass)) {
          optionsSlider.slidesToShow = 6;
          optionsSlider.vertical = true; 
          optionsSlider.verticalSwiping = true; 
        } else {
          if($(this).hasClass(selector.thumbsSmallClass)) {
            optionsSlider.slidesToShow = 10;
          }
        }
        optionsSlider.prevArrow = $(this).siblings('.slick-prev');
        optionsSlider.nextArrow = $(this).siblings('.slick-next');
        ins.preInitSlider($(this), true);
        var slider = $(this).slick(optionsSlider);
        var pg = $(this).parents(selector.wrap);
      });
    }
  },

  destroyProductThumbsSlider: function(wrap) {
    var ins = this, prefixSelector = '', selector = this.selectors.productGroup;
    if(wrap) {
      prefixSelector = wrap + ' ';
    }
    if($(prefixSelector + selector.thumbsSlider).length > 0) {
      $(prefixSelector + selector.thumbsSlider).each(function() {
        $(this).slick('unslick');
      });
    }
  },

  pauseCurrentProductVideo: function(pg) {
    if(this.data.productVideo.currentPlayer != null) {
      pg.find(this.selectors.mediaBtn).removeClass('hide');
      var currentVideoType = this.data.productVideo.currentType;
      if(currentVideoType == 'youtube' && this.data.productVideo.loadYoutubeApi) {
        this.data.productVideo.currentPlayer.pauseVideo();
        $('#' + this.data.productVideo.currentYoutubePlayerId).parents('.youtube-wrapper').addClass('hide');
      } else if(currentVideoType == 'vimeo' && this.data.productVideo.loadVimeoApi) {
        this.data.productVideo.currentPlayer.pause();
        $('#' + this.data.productVideo.currentVimeoPlayerId).addClass('hide');
      } else {
        this.data.productVideo.currentPlayer.pause();
      }
      this.data.productVideo.currentPlayer = null;
      this.data.productVideo.currentType = null;
    }
  },

  playProductVideo: function(pg, ele) {
    var selector = this.selectors.productGroup;
    var videoEle = ele.find(selector.video);
    if(videoEle.length > 0) {
      pg.find(this.selectors.mediaBtn).addClass('hide');
      if(videoEle.hasClass(selector.videoYoutube.replace('.',''))) {
        this.loadCssJsLib("youtube", function(){
          if(this.data.productVideo.loadYoutubeApi) {
            this.initYoutubePlayer(videoEle);
          } else {
            var checkYT = setInterval(function () {
              if(YT.loaded){
                clearInterval(checkYT);
                this.data.productVideo.loadYoutubeApi = true;
                this.initYoutubePlayer(videoEle);
              }
            }.bind(this), 100);
          }
        }.bind(this));
      } else if(videoEle.hasClass(selector.videoVimeo.replace('.',''))) {
        this.loadCssJsLib("vimeo", function(){
          this.data.productVideo.loadVimeoApi = true;
          this.initVimeoPlayer(videoEle);
        }.bind(this));
      } else {
        this.data.productVideo.currentType = 'custom';
        this.data.productVideo.currentPlayer = videoEle.get(0);
        this.data.productVideo.currentPlayer.play();
      }
    }
  },

  initYoutubePlayer: function(videoEle) {
    var playerId = videoEle.attr('data-player-id');
    var videoId = videoEle.attr('data-video-id');
    var playerEle = $('#' + playerId);
    playerEle.parents('.youtube-wrapper').removeClass('hide');
    videoEle.css('opacity',0);
    this.data.productVideo.currentYoutubePlayerId = playerId;
    this.data.productVideo.currentType = 'youtube';
    if(playerEle.hasClass('loaded')) {
      YT.get(playerId).playVideo();
    } else {
      playerEle.addClass('loaded');
      new YT.Player(playerId, {
        height: '480',
        width: '100%',
        playerVars :{
          autohide: 0,
          autoplay: 1,
          branding: 0,
          cc_load_policy: 0,
          controls: 1,
          fs: 1,
          iv_load_policy: 3,
          modestbranding: 1,
          playsinline: 0,
          quality: 'hd720',
          rel: 0,
          showinfo: 0,
          wmode: 'opaque'
        },
        videoId: videoId,
        events: {
          'onReady': function(event) {
            event.target.mute();
            event.target.playVideo();
          }
        }
      });
    }
    this.data.productVideo.currentPlayer = YT.get(playerId);
  },

  initVimeoPlayer: function(videoEle) {
    var iframe = $('#' + videoEle.attr('data-player-id'));
    var id = iframe.attr('id');
    this.data.productVideo.currentType = 'vimeo';
    this.data.productVideo.currentVimeoPlayerId = id;
    if(this.data.productVideo.vimeoPlayers[id] == undefined) {
      // vimeoPlayers[id] = new Vimeo.Player(iframe);
      iframe.removeClass('hide');
      var player = new Vimeo.Player(iframe.get(0), {width: 800});
      player.setVolume(0);
      player.play();
      this.data.productVideo.currentPlayer = player;
    } else {
      this.data.productVideo.currentPlayer = this.data.productVideo.vimeoPlayers[id];
      this.data.productVideo.currentPlayer.play();
    }
  },

  initProductMainSliderAndZoom: function(wrap) {
    var ins = this, prefixSelector = '', options = this.options.productGroup, selector = this.selectors.productGroup;
    if(wrap) {
      prefixSelector = wrap + ' ';
    }
    if($(prefixSelector + selector.mainsSlider).length > 0) {
      $(prefixSelector + selector.mainsSlider).each(function() {
        var optionsSlider = {};
        $.extend(true, optionsSlider, options.mainSlider);
        ins.preInitSlider($(this), true);
        var animationEle = $(this).find('.zoom-fade-slider');
        animationEle.off('webkitAnimationEnd oanimationend msAnimationEnd animationend');
        animationEle.one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
          function(e) {
          // code to execute after animation ends
          animationEle.removeClass('zoom-fade-slider');
        });
        var slider = $(this).slick(optionsSlider);
        var pg = $(this).parents(selector.wrap);
        ins.playProductVideo(pg, slider.find('.slick-current'));
        ins.initProductMainZoom(pg);
        slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
          if(pg.find(selector.thumbs).length > 0) {
            var mainLink = slick.$slider.find('.slick-current ' + selector.mainLink);
            var thumbs = pg.find(selector.thumbs);
            var thumbLink = thumbs.find(selector.thumb + ':not(slick-clone) ' + selector.thumbLink + '[data-image-id="' + mainLink.attr('data-image-id') + '"]');
            var thumb = thumbLink.parent();
            /*if(thumbs.hasClass(selector.thumbsSlider.replace('.',''))) {
              ins.gotoSliderIndex(thumbs, thumbLink.parent().attr('data-slick-index'));
            }*/
            // thumbLink.trigger('click');
            thumb.siblings('.active').removeClass('active');
            thumb.addClass('active');
            if(thumbs.hasClass(selector.thumbsSlider.replace('.',''))) {
              ins.gotoSliderIndex(thumbs, thumb.attr('data-slick-index')*1);
            }
            
          }
          ins.initProductMainZoom(pg);
          ins.pauseCurrentProductVideo(pg);
          ins.playProductVideo(pg, slick.$slider.find('.slick-current'));  
        });
      });
    } else {
      ins.initProductMainZoom($(prefixSelector + selector.wrap));
    }
  },

  destroyProductMainSliderAndZoom: function(wrap) {
    var ins = this, prefixSelector = '', selector = this.selectors.productGroup;
    if(wrap) {
      prefixSelector = wrap + ' ';
    }
    if($(prefixSelector + selector.mainsSlider).length > 0) {
      $(prefixSelector + selector.mainsSlider).each(function() {
        $(this).slick('unslick');
        var pg = $(this).parents(selector.wrap);
        ins.destroyProductMainZoom(pg);
      });
    }
  },

  destroyZoom: function(scope, imageId, size) {
    if(size == 1) {
      if(this.data.zoom[scope] != null) {
        this.data.zoom[scope].closeZoom();
        this.data.zoom[scope].destroy();
        this.data.zoom[scope] = undefined;
      }
    } else {
      if((typeof this.data.zoom[scope] == 'object') && this.data.zoom[scope] != null && this.data.zoom[scope][imageId] != undefined) {
        this.data.zoom[scope][imageId].closeZoom();
        this.data.zoom[scope][imageId].destroy();
        this.data.zoom[scope][imageId] = undefined;
      }
    }
  },

  initZoom: function(img, scope, imageId, size, checkCondition) {
    if(size == 1) {
      if((checkCondition == true && this.data.zoom[scope] == null) || checkCondition == false) {
        this.data.zoom[scope] = new CloudZoom(img, this.options.productZoom);  
      }
    } else {
      if(this.data.zoom[scope] == null) {
        this.data.zoom[scope] = {};
      }
      if((checkCondition == true && this.data.zoom[scope][imageId] == undefined) || checkCondition == false) {
        this.data.zoom[scope][imageId] = new CloudZoom(img, this.options.productZoom);  
      }
    }
  },

  initProductMainZoom: function(pg) {
    var selector = this.selectors.productGroup, ins = this, scope = pg.attr('data-zoom-scope');
    var currentSize = pg.find(selector.main + ' ' + selector.imgZoom).length;
    if(currentSize > 0 && scope != undefined) {
      pg.find(selector.main + ' ' + selector.imgZoom).each(function() {
        var img = $(this);
        img.imagesLoaded( function() {
          var imageId = img.parent().attr('data-image-id');
          if(ins.data.cacheWindowWidth > ins.options.windowScreen.mobile) {
            ins.destroyZoom(scope, imageId, currentSize);
            ins.initZoom(img, scope, imageId, currentSize, false);
          }
          $(window).on(ins.data.resizeEventName, function() {
            if(ins.data.cacheWindowWidth > ins.options.windowScreen.mobile) {
              ins.initZoom(img, scope, imageId, currentSize, true);
            } else {
              ins.destroyZoom(scope, imageId, currentSize);
            }
          });
        });  
      });
    }
  },

  destroyProductMainZoom: function(pg) {
    var selector = this.selectors.productGroup, ins = this, scope = pg.attr('data-zoom-scope');
    var currentSize = pg.find(selector.main + '.slick-current ' + selector.imgZoom).length;
    if(currentSize > 0 && scope != undefined) {
      if(ins.data.cacheWindowWidth > this.options.windowScreen.mobile) {
        this.destroyZoom(scope, imageId, currentSize);
      }
    }
  },

  changeMainImage: function(pg, orgSrc) {
    var selector = this.selectors.productGroup, ins = this;
    pg.find(selector.mainImg).each(function() {
      var image = $(this);
      var imageSize = image.attr('data-image-size');
      var newImageSrc;
      if(imageSize != undefined) {
        newImageSrc = ins.resizeImage(orgSrc, imageSize).replace('_1x1.', '_{width}x.');
      } else {
        newImageSrc = orgSrc;
      }
      image.parent(':not(.not-loading)').addClass('loading');
      image.attr('data-src', newImageSrc);
      image.addClass('lazyload');
      pg.removeClass(selector.loadingClass);
    });
  },

  updateMainThumbnail: function(pg, thumbLink, orgSrc) {
    if(thumbLink == null || thumbLink.length == 0) {
      if(orgSrc == null) {
        return;
      }
      this.changeMainImage(pg, orgSrc);
    } else {
      var selector = this.selectors.productGroup, index;
      if(pg.find(selector.mainsSlider).length > 0) { // Mains images is slider
        var mainLink = pg.find(selector.mainLink + '[data-image-id="' + thumbLink.attr('data-image-id') + '"]');
        index = mainLink.parents('.slick-slide').first().attr('data-slick-index');
        /*if(mainLink.find('img').hasClass('lazyload')) {
          mainLink.find('img').addClass('lazypreload');
        }*/
        this.gotoSliderIndex(pg.find(selector.mainsSlider), index);
      } else {
        this.changeMainImage(pg, thumbLink.attr('href'));
      }  
    }
  },

  getVariantPriceHtml: function(pg, variant) {
    var priceHtml = '';
    if(variant.compare_at_price > variant.price) {
      priceHtml += '<span class="product-price__price">' + this.getPriceHtml(variant.price) + '</span>';
      priceHtml += '<s class="product-price__price product-price__sale">' + this.getPriceHtml(variant.compare_at_price) + '</s>';
    } else {
      priceHtml += '<span class="product-price__price">' + this.getPriceHtml(variant.price) + '</span>';
    }
    if(variant.available == false && pg.attr('data-ignore-sold-out-text') == undefined) {
      priceHtml += '<span class="product-price__sold-out">' + theme.strings.soldOut + '</span>';
    }
    return priceHtml;
  },

  updateNewVariant: function(pg, newVariant, availableOption2, availableOption3, isUpdateMain, syncOtherGroup) {
    var selector = this.selectors.productGroup, options = this.options.productGroup, ins = this;
    // Update price
    pg.find(selector.price).html(this.getVariantPriceHtml(pg, newVariant));
    pg.find(selector.stickySmallPrice).html(this.getPriceHtml(newVariant.price));

    this.convertCurrencySilence(pg.find('span.money'));
    // Update swatch options
    pg.find(selector.option).each(function() {
      var index = $(this).attr('data-index');
      if($(this).hasClass('swatch')) {
        $(this).find('.selected,.hide').removeClass('selected hide');
        $(this).find(selector.optionValue).each(function() {
          if($(this).attr('data-value') == newVariant.options[index]) {
            $(this).addClass('selected');
          }
          if(index == 1 && availableOption2.indexOf($(this).attr('data-value')) == -1) {
            $(this).addClass('hide');
          } else if (index == 2 && availableOption3.indexOf($(this).attr('data-value')) == -1) {
            $(this).addClass('hide');
          }
        });
      } else {
        $(this).find(selector.optionValue).each(function() {
          if(index > 0) {
            var availableOptions;
            if(index == 1) {
              availableOptions = availableOption2;
            } else if (index == 2) {
              availableOptions = availableOption3;
            }
            $(this).find('option').each(function() {
              $(this).removeClass('hide').removeAttr('selected');
              if(availableOptions.indexOf($(this).attr('value')) == -1) {
                $(this).addClass('hide');
              }
            });
          }
          $(this).val(newVariant.options[index]);
        });
      }
    });

    // Update cart button
    if(newVariant.available) {
      var cartBtn = pg.find(selector.cartBtn);
      cartBtn.removeAttr('disabled');
      cartBtn.removeClass('soldout');
      var cartText;
      if(theme.preOrder && newVariant.inventory_policy == 'continue' && newVariant.inventory_quantity <= 0) {
        cartText = theme.strings.preOrderText;
      } else {
        cartText = theme.strings.addToCart;
      }
      cartBtn.attr('title', cartText);
      if(cartBtn.attr('data-toggle') == 'tooltip') {
        cartBtn.tooltip('fixTitle');
      }
      cartBtn.find('span').text(cartText);
    } else {
      var cartBtn = pg.find(selector.cartBtn);
      cartBtn.attr('disabled','disabled');
      cartBtn.addClass('soldout');
      cartBtn.attr('title', theme.strings.soldOut);
      if(cartBtn.attr('data-toggle') == 'tooltip') {
        cartBtn.tooltip('fixTitle');
      }
      cartBtn.find('span').text(theme.strings.soldOut);
    }

    // Update variant id and title
    pg.find(selector.variantId).val(newVariant.id);
    pg.find(selector.variantId).trigger('change');
    if(pg.find(selector.variantTitle).length > 0) {
      pg.find(selector.variantTitle).text(newVariant.title);
    }
    
    // Filter small and big images
    var currentThumbLink = pg.find(selector.thumbLink + '[data-image-id="' + newVariant.featured_media.id + '"]');
    
    // Update main image
    if(isUpdateMain) {
      this.updateMainThumbnail(pg, currentThumbLink, newVariant.featured_media.src);
    }

    // Update history
    if(pg.attr('data-history') != undefined) {
      var hrefParts = window.location.href.split('?');
      var newUrl = hrefParts[0] + '?';
      if(hrefParts.length > 1 && hrefParts[1]) {
        if(hrefParts[1].indexOf('variant') > -1) {
          newUrl += hrefParts[1].replace(/(variant=).*?(&|$)/,'$1' + newVariant.id + '$2');
        } else {
          newUrl += hrefParts[1] + '&variant=' + newVariant.id;
        }
      } else {
        newUrl += 'variant=' + newVariant.id;
      }
      // var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?variant=' + newVariant.id;
      window.history.replaceState({path: newUrl}, '', newUrl);  
    }

    // Update deal label
    if(pg.find(selector.labelDeal).length > 0) {
      var label = pg.find(selector.labelDeal);
      if(newVariant.compare_at_price && newVariant.compare_at_price > newVariant.price) {
        label.removeClass('hide');
        if(label.find(selector.labelPercent).length > 0) {
          var percentNumber = Math.floor(((newVariant.compare_at_price - newVariant.price)/newVariant.compare_at_price)*100);
          label.find(selector.labelPercent).html(percentNumber + '%');
        }
      } else {
        label.addClass('hide');
      }
    }
    if(pg.find(selector.stockText).length > 0) {
      if(newVariant.inventory_quantity > 0) {
        pg.find(selector.stockText1).removeClass('hide');
        pg.find(selector.stockText2).addClass('hide');
      } else {
        pg.find(selector.stockText2).removeClass('hide');
        pg.find(selector.stockText1).addClass('hide');
      }
      if(pg.find(selector.stockNumber).length > 0) {
        pg.find(selector.stockNumber).text(newVariant.inventory_quantity);
      }
    }

    // Sync the variant
    if(pg.find(selector.syncUrl).length > 0) {
      var syncUrl = pg.find(selector.syncUrl).attr('href').split('?')[0];
      var newSyncUrl = syncUrl + '?variant=' + newVariant.id;
      pg.find(selector.syncUrl).attr('href', newSyncUrl);
    }

    // Update SKU
    var skuTag = pg.find(selector.sku);
    if(skuTag.length > 0) {
      if(newVariant.sku) {
        skuTag.text(newVariant.sku);
        skuTag.parent().removeClass('hide');
      } else {
        skuTag.parent().addClass('hide');
      }
    }

    // Sync to other same pg
    if(pg.attr('data-sync-pg') != undefined && syncOtherGroup) {
      $(pg.attr('data-sync-pg')).each(function() {
        $(this).find(selector.groupData).attr('data-value',pg.find(selector.groupData).attr('data-value'));
        ins.updateNewVariant($(this), newVariant, availableOption2, availableOption3, true, false);
      });
    }   
  },

  initMainLightboxGallery: function() {
    var selector = this.selectors.productGroup, ins = this;
    $(document).on('click', selector.mainLink, function(e) {
      e.preventDefault();
      $(this).parents(selector.wrap).find(ins.selectors.mediaBtn).trigger('click',[$(this).attr('data-image-id')]);
    });
  },

  initProductGroup: function() {
    var selector = this.selectors.productGroup, options = this.options.productGroup, ins = this;
    // Init small images slider
    // this.initProductThumbsSlider();
    // this.initProductMainSliderAndZoom();
    
    $(document).on('click change', selector.optionValue, function(e) {
      e.preventDefault();
      var btn = $(this);
      var pg = btn.parents(selector.wrap).first();
      var productHandle = pg.attr('data-product-handle');
      if($(this).hasClass('disabled') || ($(this).hasClass('selected') && ins.data.productGroup[productHandle] != undefined) || (e.type == 'click' && e.target.nodeName == 'SELECT')) {
        return;
      }
      new Promise(function(resolve, reject) {
        if(ins.data.productGroup[productHandle] == undefined) {
          // Load product data
          pg.addClass(selector.loadingClass);
          ins.callAjax(theme.rootUrl + 'products/' + productHandle, 'get', {view: 'tjson'}, null, function(response) {
            ins.data.productGroup[productHandle] = $.parseJSON(response.replace('<!-- BEGIN template -->','').replace('<!-- product.tjson -->','').replace('<!-- END template -->',''));
            pg.removeClass(selector.loadingClass);
            resolve();
          });
        } else {
          resolve();
        }
      }).then(function() {
        // Create current select
        var currentOption = btn.parents(selector.option);
        var currentOptionIndex = currentOption.attr('data-index');
        var currentSelect = [];
        var optionFormat = btn.prop("tagName").toLowerCase();
        pg.find(selector.option).each(function() {
          var index = $(this).attr('data-index'), value;
          if(index != currentOptionIndex) {
            if($(this).hasClass('swatch')) {
              value = $(this).find('.selected').attr('data-value');
            } else {
              value = $(this).find(selector.optionValue).val();
            }
          } else {
            if(optionFormat == 'a') {
              value = btn.attr('data-value');
            } else {
              value = btn.val();
            }
          }
          currentSelect.push(value);
          if($(this).hasClass(selector.isGroupClass)) {
            pg.find(selector.groupData).attr('data-value', value);
          }
        });

        // Find new variant base on current select
        var optionSize = currentSelect.length,
          newVariant,
          newVariant1,
          newVariant2,
          newVariant3,
          availableOption2 = [],
          availableOption3 = [];
        $.each(ins.data.productGroup[productHandle], function(variantId, variant) {
          if(variant.options[0] == currentSelect[0]) {
            newVariant1 = variant;
            if(optionSize > 1 && availableOption2.indexOf(variant.options[1]) == -1) {
              availableOption2.push(variant.options[1]);
            }
          }
          if(optionSize > 1
              && variant.options[0] == currentSelect[0] 
              && variant.options[1] == currentSelect[1]) {
            if(newVariant2 == undefined) {
              newVariant2 = variant;
            }
            if(optionSize > 2 && availableOption3.indexOf(variant.options[2]) == -1) {
              availableOption3.push(variant.options[2]);
            }
          }
          if(optionSize > 2 && newVariant3 == undefined
              && variant.options[0] == currentSelect[0] 
              && variant.options[1] == currentSelect[1]
              && variant.options[2] == currentSelect[2]) {
            newVariant3 = variant;
          }
        });
        if(newVariant3 != undefined) {
          newVariant = newVariant3;
        } else if (newVariant2 != undefined) {
          newVariant = newVariant2;
        } else {
          newVariant = newVariant1;
        }
        ins.updateNewVariant(pg, newVariant, availableOption2, availableOption3, true, true);
      }, function(err) {
        // console.log(err);
      });
    });

    // Init click event for product thumbs
    $(document).on('click', selector.thumbLink, function(e) {
      e.preventDefault();
      if($(this).parent().hasClass('active')) {
        return;
      }
      var currentThumbLink = $(this);
      var currentImageId = currentThumbLink.attr('data-image-id');
      var pg = currentThumbLink.parents(selector.wrap);
      var isHuman = false;
      if(e.originalEvent != undefined) {
        isHuman = true;
      }
      currentThumbLink.parent().addClass('active').siblings('.active').removeClass('active');
      if(pg.find(selector.option).length > 0) {
        var productHandle = pg.attr('data-product-handle');
        new Promise(function(resolve, reject) {
          if(ins.data.productGroup[productHandle] == undefined) {
            // Load product data
            pg.addClass(selector.loadingClass);
            ins.callAjax(theme.rootUrl + 'products/' + productHandle, 'get', {view: 'tjson'}, null, function(response) {
              ins.data.productGroup[productHandle] = $.parseJSON(response.replace('<!-- BEGIN template -->','').replace('<!-- product.tjson -->','').replace('<!-- END template -->',''));
              pg.removeClass(selector.loadingClass);
              resolve();
            });
          } else {
            resolve();
          }
        }).then(function(){
          var newVariant, availableOption2 = [], availableOption3 = [], groupOptionIndex, groupOptionValue;
          var newVariantPriority;
          var currentPriorityPoint = 0;
          var currentSelect = [];
          pg.find(selector.option).each(function() {
            var index = $(this).attr('data-index'), value;
            if($(this).hasClass('swatch')) {
              value = $(this).find('.selected').attr('data-value');
            } else {
              value = $(this).find(selector.optionValue).val();
            }
            currentSelect.push(value);
          });
          $.each(ins.data.productGroup[productHandle], function(index, variant) {
            if(variant.featured_media.id == currentImageId) {
              newVariant = variant;
              if(groupOptionValue != undefined && groupOptionValue == variant.options[groupOptionIndex]) {
                newVariantPriority = variant;
                return;
              } else {
                var priorityPoint = 0;
                $.each(variant.options, function(optionIndex, value) {
                  if(currentSelect[optionIndex] == value) {
                    priorityPoint += (3 - optionIndex);
                  }
                });
                if(priorityPoint > currentPriorityPoint) {
                  newVariantPriority = variant;
                  currentPriorityPoint = priorityPoint;
                }
              }
            }
          });
          if(newVariantPriority != undefined) {
            newVariant = newVariantPriority;
          }
          if(newVariant != undefined) {
            var currentSelect = newVariant.options;
            var optionSize = currentSelect.length;
            $.each(ins.data.productGroup[productHandle], function(variantId, variant) {
              if(variant.options[0] == currentSelect[0]) {
                if(optionSize > 1 && availableOption2.indexOf(variant.options[1]) == -1) {
                  availableOption2.push(variant.options[1]);
                }
              }
              if(optionSize > 1
                  && variant.options[0] == currentSelect[0] 
                  && variant.options[1] == currentSelect[1]) {
                if(optionSize > 2 && availableOption3.indexOf(variant.options[2]) == -1) {
                  availableOption3.push(variant.options[2]);
                }
              }
            });
            ins.updateNewVariant(pg, newVariant, availableOption2, availableOption3, isHuman, true);
          } else if(isHuman) {
            if(!currentThumbLink.hasClass('loaded')) {
              currentThumbLink.addClass('loaded');
              pg.addClass(selector.loadingClass);
            }
            ins.updateMainThumbnail(pg, currentThumbLink);
          }
        }, function(err){
          // console.log(err);
        });
      } else {
        if(isHuman) {
          ins.updateMainThumbnail(pg, currentThumbLink);
        }
      }
    });
  },

  openLightboxByPhotoswipe: function(pg, currentImageId) {
    var ins = this, selector = this.selectors.productGroup;
    if(pg.find(selector.mainLink).length > 0) {
      var items = [];
      var activeIndex = 0;
      pg.find(selector.mainLink + ':not(.hide)').each(function(index, element) {
        var ele = $(this);
        var title = ele.attr('title');
        if(title) {
          title = title.replace(/ group-(.*?) /g, ' ').replace(/group-(.*?) /g, '').split('group')[0];
        }
        if(currentImageId != undefined) {
          if($(this).attr('data-image-id') == currentImageId) {
            activeIndex = index;
          }
        } else if($(this).parent().hasClass('active')) {
          activeIndex = index;
        }
        items.push({
          src: ele.attr('href'),
          w: ele.attr('data-width'),
          h: ele.attr('data-height'),
          msrc: ins.resizeImage(ele.attr('href'), '96x'),
          title: title,
          el: ele.parent()[0]
        });
      });
      var pswpElement = document.querySelectorAll('.pswp')[0];
      // define options (if needed)
      var options = {
          // optionName: 'option value'
          // for example:
          index: activeIndex, // start at first slide,
          getThumbBoundsFn: function(index) {
            // See Options -> getThumbBoundsFn section of documentation for more info
            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                rect = thumbnail.getBoundingClientRect(); 

            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
          },
          shareButtons: []
      };
      var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
      gallery.init();
    }
  },

  initMediaButton: function() {
    var ins = this, selector = this.selectors.productGroup, working = false;
    $(document).on('click', this.selectors.mediaBtn, function(e, currentImageId) {
      e.preventDefault();
      if(working) {
        return;
      }
      working = true;
      var pg = $(this).parents(selector.wrap), btn = $(this);
      ins.startLoadingBtn(btn);
      ins.showLoadingFull();
      ins.loadSnippetAjax('photoswipe', function() {
        ins.loadCssJsLib('photoswipe', function() {
          ins.endLoadingBtn(btn);
          ins.hideLoadingFull();
          ins.openLightboxByPhotoswipe(pg, currentImageId);
          working = false;
        });
      }, 700);
    });
  },

  initQtyBox: function() {
    var selector = this.selectors.qtyBox;
    $(document).on('click', selector.ctrlBtn, function(e) {
      e.preventDefault();
      var input = $(this).siblings('input').first();
      var value = parseInt(input.val());
      if($(this).hasClass(selector.decreaseBtnClass)) {
        if(value > 1) {
          input.val(value - 1);
          if(input.hasClass(selector.inputSyncClass)) {
            input.trigger('change');
          }
        }
      } else {
        input.val(value + 1);
        if(input.hasClass(selector.inputSyncClass)) {
          input.trigger('change');
        }
      }
    });
  },

  runCountDown: function (ele, timeValue, pattern) {
    var tplHour = (ele.attr('data-tpl-hour') != undefined) ? ($(ele.attr('data-tpl-hour')).html()) : theme.strings.countdownTplHour;
    var tplDay = (ele.attr('data-tpl-day') != undefined) ? ($(ele.attr('data-tpl-day')).html()) : theme.strings.countdownTplDay;
    var tplWeek = (ele.attr('data-tpl-week') != undefined) ? ($(ele.attr('data-tpl-week')).html()) : theme.strings.countdownTplWeek;

    var ins = this;
    ele.countdown(timeValue)
    .on('update.countdown', function(event) {
      var format = tplHour;
      if(event.offset.totalDays > 0) {
        format = tplDay;
      }
      if(event.offset.weeks > 0) {
        format = tplWeek;
      }
      $(this).html(event.strftime(format));
    })
    .on('finish.countdown', function(event) {
      var extend = parseInt(ele.attr('data-extend'));
      if(pattern == 1 && extend != undefined && Number.isInteger(extend) && extend > 0) {
        var productId = ele.data('product-id');
        var cookieName = 'countdown_extend_' + productId;
        var date = new Date();
        var times = date.getTime() + extend*86400*1000;
        ins.setCookie(cookieName, times, null);
        date.setTime(times);
        timeValue = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate()
          + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        ins.runCountDown(ele, timeValue, 1);
      } else {
        $(this).html(theme.strings.countdownExpiredMsg);
        // .parent().addClass('disabled');
        // $(this).parent().addClass('hide');
      }
    });
  },

  initReverseCountdown: function(ele, reverse, productId) {
    var cookieName = 'countdown_reverse_' + productId, ins = this;
    var cookieCountdown = this.getCookie(cookieName), times;
    if(cookieCountdown == undefined || cookieCountdown == NaN || cookieCountdown == 'NaN' || cookieCountdown == 'null' || cookieCountdown == null) {
      var timesText = reverse.split(':');
      if(timesText.length == 4) {
        var days = parseInt(timesText[0]);
        var hours = parseInt(timesText[1]);
        var minutes = parseInt(timesText[2]);
        var seconds = parseInt(timesText[3]);
        if(hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59) {
          var date = new Date();
          times = date.getTime() + (days*86400 + hours*3600 + minutes*60 + seconds)*1000;
        }
      }
      this.setCookie(cookieName, times, null);
    } else {
      times = parseInt(cookieCountdown);
    }
    if(times != undefined) {
      var currentDate = new Date();
      var currentTime = currentDate.getTime();
      if(times <= currentTime - 30*60) {
        times = null;
        var timesText = reverse.split(':');
        if(timesText.length == 4) {
          var days = parseInt(timesText[0]);
          var hours = parseInt(timesText[1]);
          var minutes = parseInt(timesText[2]);
          var seconds = parseInt(timesText[3]);
          if(hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59) {
            times = currentTime + (days*86400 + hours*3600 + minutes*60 + seconds)*1000;
          }
        }
        this.setCookie(cookieName, times, null);
      }
      if(times) {
        currentDate.setTime(times);
        var timeValue = currentDate.getFullYear() + '/' + (currentDate.getMonth() + 1) + '/' + currentDate.getDate()
          + ' ' + currentDate.getHours() + ':' + currentDate.getMinutes() + ':' + currentDate.getSeconds();
        this.runCountDown(ele, timeValue, 2);
      }
    }
  },

  initDealCountdown: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var ins = this;
    if(wrap.find(this.selectors.productGroup.countdown).length > 0) {
      wrap.find(this.selectors.productGroup.countdown).each(function() {
        var dataDeal = $(this).attr('data-timer');
        var pattern1 = /^\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}$/;
        var pattern2 = /^\d{1,2}:\d{2}:\d{2}:\d{2}$/;

        if(dataDeal != undefined && dataDeal != '') {
          if(pattern1.test(dataDeal) == true) {
            ins.runCountDown($(this), dataDeal, 1);  
          } else if(pattern2.test(dataDeal)) {
            var productId = $(this).data('product-id');
            ins.initReverseCountdown($(this), dataDeal, productId);
          }
        } 
      });
    }
  },

  destroyDealCountdown: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    if(wrap.find(this.selectors.productGroup.countdown).length > 0) {
      wrap.find(this.selectors.productGroup.countdown).each(function() {
        $(this).countdown("stop");
      });
    }
  },

  reLoadReview: function (wrap) {
    if(!theme.review.enable) {
      return;
    }
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    if(theme.review.app == 'product_review') {
      if(typeof SPR != 'undefined' && typeof SPR.$ != 'undefined') {
        return SPR.registerCallbacks(), SPR.initRatingHandler(), SPR.initDomEls(), SPR.loadProducts(), SPR.loadBadges();
      }
    }
  },

  runProgressBar: function(progressBar) {
    if(progressBar.length > 0) {
      progressBar.width(progressBar.data('width') + '%');
    }
  },

  loadSingleGeneralCss: function(cb) {
    var snippetName = 'preload-product-single-general.scss';
    if(this.data.loadedSnippets.indexOf(snippetName) > -1) {
      cb();
    } else {
      var ins = this;
      this.callAjax(theme.searchUrl, 'GET', {view: 'snippet', q: snippetName}, null, function(html) {
        ins.data.loadedSnippets.push(snippetName);
        $('<style rel="stylesheet"></style>').html(html).appendTo($('head'));
        cb();
      });
    }
  },

  // Quick view popup
  initQuickView: function() {
    var ins = this, qvPopupSelector = this.selectors.qvPopup, workingClass = this.selectors.workingClass;
    $(document).on('click', this.selectors.qvBtn, function(e) {
      e.preventDefault();
      if($(this).hasClass(workingClass)) {
        return;
      }
      $(this).addClass(workingClass);
      var url = $(this).attr('href'), btn = $(this);
      ins.startLoadingBtn(btn);
      ins.loadCssJsLib('quickViewPopup');
      
      ins.loadSingleGeneralCss(function() {
        ins.loadSnippetAjax('popup-quickview', function() {
          ins.callAjax(url, 'GET', {view: 'quick_view'}, null, function(html) {
            var jsonScript = $('.product-single-json', html);
            if(jsonScript.length > 0) {
              ins.data.productGroup[jsonScript.attr('data-handle')] = $.parseJSON(jsonScript.html());
              ins.addProductRecentView(jsonScript.attr('data-handle'));
            }
            $(qvPopupSelector).find('.product-section-content').html(ins.stripScripts(html));
            ins.convertCurrencySilence(qvPopupSelector + ' span.money');
            ins.applyCustomColorSwatches(qvPopupSelector);
            ins.endLoadingBtn(btn);
            ins.showPopup(qvPopupSelector);
            ins.rteWrap(qvPopupSelector + ' .rte');
            btn.removeClass(workingClass);
            ins.reLoadReview(qvPopupSelector);
            ins.runProgressBar($(qvPopupSelector + ' .progress-bar'));
            ins.popularAddedWishlistItems(qvPopupSelector);
          });
        });
      });
    });
    $(document).on('show.bs.modal',qvPopupSelector, function () {
      setTimeout(function() {
        ins.initProductThumbsSlider(qvPopupSelector);
        ins.initProductMainSliderAndZoom(qvPopupSelector);
      }, 1000);
      ins.initDealCountdown(qvPopupSelector);
    });
    $(document).on('hidden.bs.modal',qvPopupSelector, function () {
      ins.destroyProductMainSliderAndZoom(qvPopupSelector);
      ins.destroyProductThumbsSlider(qvPopupSelector);
      ins.destroyDealCountdown(qvPopupSelector);
    });
  },

  initSelectOption: function() {
    var ins = this, selectOptionPopupSelector = this.selectors.selectOptionPopup, workingClass = this.selectors.workingClass;
    $(document).on('click', this.selectors.selectOptionBtn, function(e) {
      e.preventDefault();
      if($(this).hasClass(workingClass)) {
        return;
      }
      $(this).addClass(workingClass);
      var url = $(this).attr('href'), btn = $(this);
      ins.startLoadingBtn(btn);
      ins.loadSingleGeneralCss(function() {
        ins.loadSnippetAjax('popup-select-option', function() {
          ins.callAjax(url, 'GET', {view: 'select_option'}, null, function(html) {
            var jsonScript = $('.product-single-json', html);
            if(jsonScript.length > 0) {
              ins.data.productGroup[jsonScript.attr('data-handle')] = $.parseJSON(jsonScript.html());
            }
            $(selectOptionPopupSelector).find('.product-section-content').html(ins.stripScripts(html));
            ins.convertCurrencySilence(selectOptionPopupSelector + ' span.money');
            ins.applyCustomColorSwatches(selectOptionPopupSelector);
            ins.endLoadingBtn(btn);
            ins.showPopup(selectOptionPopupSelector);
            btn.removeClass(workingClass);
          });
        });
      });
    });
  },

  // Comparison popup
  getCookieItemsValue: function(returnArr, cookieName) {
    if(typeof this.data.cookieArray[cookieName] == 'undefined') {
      var handleStr = this.getCookie(cookieName);
      if(handleStr == undefined || handleStr == '') {
        this.data.cookieArray[cookieName] = [];
      } else {
        this.data.cookieArray[cookieName] = handleStr.split(',');
      }
    }
    if(returnArr) {
      return this.data.cookieArray[cookieName];
    }
    return this.data.cookieArray[cookieName].join(',');
  },

  isProductExistInCookie: function(productHandle, cookieName) {
    var items = this.getCookieItemsValue(true, cookieName);
    return items.indexOf(productHandle) >= 0;
  },

  addProductToCookie: function(productHandle, cookieName) {
    var items = this.getCookieItemsValue(true, cookieName);
    if(items.indexOf(productHandle) < 0) {
      items.push(productHandle);
      this.data.cookieArray[cookieName] = items;
      this.setCookie(cookieName, items.join(','), null);
    }
  },
  
  removeProductFromCookie: function(productHandle, cookieName) {
    if(this.data.cookieArray[cookieName] != undefined && this.data.cookieArray[cookieName].indexOf(productHandle) >= 0) {
      this.data.cookieArray[cookieName].splice(this.data.cookieArray[cookieName].indexOf(productHandle), 1);
      this.setCookie(cookieName, this.data.cookieArray[cookieName].join(','), null);
    }
  },

  loadCompare: function(cb) { 
    var ins = this;
    this.loadCssJsLib('quickViewPopup');
    this.loadSingleGeneralCss(function() {
      ins.callAjax(theme.searchUrl, 'GET', {view: 'compare', q: ins.getCookieItemsValue(false, ins.options.compare.cookieName)}, null, function(html) {
        ins.loadSnippetAjax('popup-compare', function() {
          var popupContent = $(ins.selectors.compare.popup).find('.compare-content');
          popupContent.html(ins.stripScripts(html));
          ins.rteWrap(ins.selectors.compare.popup + ' .rte');
          ins.applyCustomColorSwatches(popupContent);
          ins.convertCurrencySilence(popupContent.find('span.money'));
          ins.findBestPriceInCompare();
          ins.reLoadReview(popupContent);
          ins.firstLoaded = true;
          if(cb) {
            cb();
          }
        });
      });
    });
  },

  findBestPriceInCompare: function() {
    var selector = this.selectors.compare, ins = this;
    var badges = $(selector.bestPriceBadge), index = -1, bestPrice = 0, popup = $(selector.popup);
    if(badges.length > 0) {
      badges.each(function() {
        var badge = $(this);
        var currentIndex = badge.parents('td').attr('data-index');
        var available = popup.find('td[data-index="' + currentIndex + '"]' + selector.availabilityRow + ' ' + ins.selectors.productGroup.outstockText).hasClass('hide');
        var price = parseFloat($(this).attr('data-price'));
        if(available && (bestPrice == 0 || price < bestPrice)) {
          bestPrice = price;
          index = $(this).parents('td').attr('data-index');
        }
      });
      badges.addClass('hide');
      $(selector.popup + ' td[data-index="' + index + '"] ' + selector.bestPriceBadge).removeClass('hide');
    }
  },

  afterUpdatedVariantCompare: function(selectVariant) {
    var selector = this.selectors.productGroup;
    var index = selectVariant.attr('data-index'), pg = selectVariant.parents(selector.wrap), table = selectVariant.parents('table'), variantId = selectVariant.val();
    var variants = this.data.productGroup[pg.attr('data-product-handle')];
    if(variants != undefined) {
      var newVariant = variants[variantId];
      if(newVariant != undefined) {
        // Update price
        var priceCol = table.find('td[data-index="' + index + '"] ' + selector.price);        
        priceCol.html(this.getVariantPriceHtml(pg, newVariant));    
        this.convertCurrencySilence(priceCol.find('span.money'));
        priceCol.parent().find(this.selectors.compare.bestPriceBadge).attr('data-price', newVariant.price);
        
        // Update main image
        if(newVariant.featured_media != null && newVariant.featured_media.src != null) {
          var img = table.find('td[data-index="' + index + '"] ' + selector.mainImg);
          img.parent(':not(.not-loading)').addClass('loading');
          img.attr('data-src', this.resizeImage(newVariant.featured_media.src, img.attr('data-image-size')).replace('_1x1.', '_{width}x.'));
          img.addClass('lazyload');
        }

        // Update actions and availability row
        var actionsRow = table.find('td[data-index="' + index + '"]' + this.selectors.compare.actionsRow);
        var availabilityRow = table.find('td[data-index="' + index + '"]' + this.selectors.compare.availabilityRow);
        var cartBtn = actionsRow.find(this.selectors.cart.addBtn);
        if(newVariant.available) {
          cartBtn.removeAttr('disabled');
          cartBtn.removeClass('soldout');
          cartBtn.attr('title', theme.strings.addToCart).tooltip('fixTitle');
          cartBtn.find('span').text(theme.strings.addToCart);
        } else {
          cartBtn.attr('disabled', 'disabled');
          cartBtn.addClass('soldout');
          cartBtn.attr('title', theme.strings.soldOut).tooltip('fixTitle');
          cartBtn.find('span').text(theme.strings.soldOut);
        }
        actionsRow.find(selector.variantId).val(variantId);

        this.findBestPriceInCompare();
      }
    }
  },

  initCompareEvent: function() {
    var ins = this, workingClass = 'working';
    $(document).on('click', this.selectors.compare.btn, function(e) {
      e.preventDefault();
      var handle = $(this).attr('data-handle'), btn = $(this);
      if(ins.isProductExistInCookie(handle, ins.options.compare.cookieName) && $(ins.selectors.compare.popup).find('table').length > 0) {
        ins.showPopup(ins.selectors.compare.popup);
      } else {
        ins.addProductToCookie(handle, ins.options.compare.cookieName);
        ins.startLoadingBtn(btn);
        ins.loadCompare(function() {
          ins.endLoadingBtn(btn);
          ins.showPopup(ins.selectors.compare.popup);
        });  
      }
    });

    $(document).on('click', this.selectors.compare.removeBtn, function(e) {
      e.preventDefault();
      var handle = $(this).attr('data-handle');
      ins.removeProductFromCookie(handle, ins.options.compare.cookieName);
      ins.loadCompare();
    });

    $(document).on('click', this.selectors.compare.headerLink, function(e) {
      e.preventDefault();
      var btn = $(this);
      if(btn.hasClass(workingClass)) {
        return;
      }
      btn.addClass(workingClass);
      if(ins.firstLoaded) {
        ins.showPopup(ins.selectors.compare.popup);
        btn.removeClass(workingClass);
      } else {
        ins.showLoadingFull();
        ins.loadCompare(function() {
          ins.hideLoadingFull();
          ins.showPopup(ins.selectors.compare.popup);
          btn.removeClass(workingClass);
        });  
      }
    });
    
    $(document).on('change', this.selectors.compare.variantId, function(e) {
      ins.afterUpdatedVariantCompare($(this));
    });
  },

  loadDynamicProductsAjax: function(params, page, cb) {
    var data = {
      'view': 'dynamic_ajax',
      'q': params,
      'page': page
    }
    this.callAjax(theme.searchUrl, 'GET', data, null, cb);
  },

  afterLoadDynamicProductsAjaxInTab: function(parent, html) {
    var ins = this;
    var hasSlider = parent.parent().find(this.selectors.slider.default).length > 0;
    if(hasSlider) {
      parent.css('opacity', 0);
    }
    parent.removeClass(this.selectors.waitingData.replace('.',''));
    parent.append($('.products', html).html());
    this.convertCurrencySilence(parent.find('span.money'));
    
    var loadMoreBtn = parent.siblings(this.selectors.loadMoreBtn);
    if(loadMoreBtn.length > 0) {
      loadMoreBtn.remove();
    }
    if($(this.selectors.loadMoreBtn, html).length > 0) {
      parent.parent().append($(this.selectors.loadMoreBtn, html).get(0).outerHTML);
    }
    ins.initSlider(parent.parents('.tab-pane'), false, false);
    this.initDealCountdown(parent.parent());
    this.applyCustomColorSwatches(parent);
    this.reLoadReview(parent);
    this.popularAddedWishlistItems(parent);
    if(hasSlider) {
      parent.animate({'opacity': 1}, { 
        duration: 500,
        delay: 300
      });
    }
  },

  destroyInfiniteScroll: function(idSave) {
    if(typeof this.data.productGroupInfiniteScroll[idSave] != 'undefined') {
      $.each(this.data.productGroupInfiniteScroll[idSave], function(index, obj) {
        obj.destroy();
      });
    }
  },

  initInfiniteScroll: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    if(wrap.find(this.selectors.infiniteScroll.button).length > 0) {
      var ins = this;
      wrap.find(this.selectors.infiniteScroll.button).each(function() {
        var dataIdSave = $(this).attr('data-id-save');
        if(typeof ins.data.productGroupInfiniteScroll[dataIdSave] == 'undefined') {
          ins.data.productGroupInfiniteScroll[dataIdSave] = [];
        }
        var element = $(this).siblings('.products').first();
        var dataId = $(this).attr('data-id');
        ins.data.productGroupInfiniteScroll[dataIdSave].push(new Waypoint.Infinite({
          element: element,
          items: '.grid__item',
          more: dataId,
          onAfterPageLoad: function($items) {
            ins.applyCustomColorSwatches($items);
            BtCurrency.convertSilence(shopCurrency, BtCurrency.currentCurrency, $items.find('span.money'));
            ins.initDealCountdown($items);
            ins.reLoadReview($items);
          }
        }));  
      });       
    }
  },

  initProductTabs: function(wrap, sectionId) {
    var ins = this;
    var firstSliderWrap;
    if(typeof wrap == 'string') {
      firstSliderWrap = wrap + ' .tab-pane:first';
      wrap = $(wrap);
    } else {
      firstSliderWrap = wrap.find('.tab-pane:first');
    }
    this.convertCurrencySilence(wrap.find('span.money'));
    this.initScrollingWindowTriggerOnce(wrap, 'product-tabs_deal_slider_' + sectionId, -170, function() {
      ins.initDealCountdown(wrap);
      ins.initSlider(firstSliderWrap, false, false);
    });
    this.initInfiniteScroll(wrap);
    wrap.find(this.selectors.waitingScrollData).each(function() {
      var trigger = $(this);
      ins.initScrollingWindowTriggerOnce(trigger, 'product-tabs_ajax_' + sectionId, -370, function() {
        var parent = trigger.parent();
        ins.loadDynamicProductsAjax(trigger.attr('data-ajax-params'), 1, function(html) {
          trigger.remove();
          ins.afterLoadDynamicProductsAjaxInTab(parent, html);
        });
      });
    });
    wrap.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var tabContent = $(e.target).attr('href');
      var waitingObjs = $(tabContent).find(ins.selectors.waitingTabData);
      if(waitingObjs.length > 0) {
        waitingObjs.each(function() {
          var trigger = $(this);
          var parent = trigger.parent();
          ins.loadDynamicProductsAjax(trigger.attr('data-ajax-params'), 1, function(html) {
            trigger.remove();
            ins.afterLoadDynamicProductsAjaxInTab(parent, html);
          });
        });
      } else if ($(tabContent).find(ins.selectors.infiniteScroll.wait).length > 0) {
        $(tabContent).find(ins.selectors.infiniteScroll.wait).removeClass(ins.selectors.infiniteScroll.wait.replace('.','')).addClass(ins.selectors.infiniteScroll.button.replace('.',''));
        ins.initInfiniteScroll($(tabContent));
      }
    });
  },

  unLoadProductTabs: function(wrap) {
    var sectionId = wrap.attr('data-section-id');
    this.destroyInfiniteScroll(wrap.attr('data-section-id'));
    this.destroyScrollingWindowTriggerOnce('product-tabs_ajax_' + sectionId);
    this.destroyScrollingWindowTriggerOnce('product-tabs_deal_slider_' + sectionId);
    this.destroyDealCountdown(wrap);
  },

  invertColor: function(hex, bw) {
      if (hex.indexOf('#') === 0) {
          hex = hex.slice(1);
      }
      // convert 3-digit hex to 6-digits.
      if (hex.length === 3) {
          hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }
      if (hex.length !== 6) {
        return false;
      }
      var r = parseInt(hex.slice(0, 2), 16),
          g = parseInt(hex.slice(2, 4), 16),
          b = parseInt(hex.slice(4, 6), 16);
      if (bw) {
          // http://stackoverflow.com/a/3943023/112731
          return (r * 0.299 + g * 0.587 + b * 0.114) > 186
              ? '#000000'
              : '#FFFFFF';
      }
      // invert color components
      r = (255 - r).toString(16);
      g = (255 - g).toString(16);
      b = (255 - b).toString(16);
      // pad each with zeros and return
      return "#" + this.padZero(r) + this.padZero(g) + this.padZero(b);
  },

  padZero: function(str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
  },

  loadCustomColorCodes: function(cb) {
    if(this.data.customColorCodes != null || this.data.customColorCodesLoading) {
      cb();
    } else if(this.data.customColorCodesLoading == false) {
      var ins = this;
      this.data.customColorCodesLoading = true;
      this.callAjax(theme.rootUrl, 'get', {view:'color_codes'}, null, function(response) {
        ins.data.customColorCodes = $.parseJSON(response.replace('<!-- BEGIN template --><!-- index.color_codes -->','').replace('<!-- END template -->',''));
        ins.data.customColorCodesLoading = false;
        if(ins.data.customColorCodesStack.length > 0) {
          $.each(ins.data.customColorCodesStack, function(index, value) {
            ins.runCustomColorCodes(value);
            delete ins.data.customColorCodesStack[index];
          });
        }
        cb();
      });
    }
  },

  runCustomColorCodes: function(wrap) {
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var ins = this;
    wrap.find('.color-load:not(.color-loaded)').each(function() {
      var colorTag = $(this).attr('data-color-tag');
      if(colorTag != undefined) {
        var swatchData = ins.data.customColorCodes[colorTag];
        if(swatchData != undefined) {
          if(swatchData.color != undefined) {
            $(this).css('background-color', swatchData.color);
            if(!$(this).hasClass('ignore-color-text')) {
              var newColor = ins.invertColor(swatchData.color, true);
              if(newColor) {
                $(this).css('color', newColor);
              }
            }
          }
          if(swatchData.image != undefined) {
            var imageUrl = ins.resizeImage(swatchData.image, '46x46_crop_center');
            $(this).css('background-image', 'url(' + imageUrl + ')');
          } 
        }
      }
      $(this).addClass('color-loaded');
    });
  },

  applyCustomColorSwatches: function(wrap) {
    var ins = this;
    this.loadCustomColorCodes(function() {
      if(ins.data.customColorCodesLoading) {
        ins.data.customColorCodesStack.push(wrap);
      } else {
        ins.runCustomColorCodes(wrap);
      }
    });
  },

  initLoadMoreBtn: function() {
    var ins = this;
    $(document).on('click', this.selectors.loadMoreBtn, function(e) {
      e.preventDefault();
      var btn = $(this);
      if(btn.hasClass(ins.selectors.workingClass)) {
        return;
      }
      btn.addClass(ins.selectors.workingClass).prepend('<i class="' + ins.selectors.loadingAjaxClass + '" style="margin-right: 5px;"></i>');
      var href = $(this).attr('data-href');
      var parent = $(this).siblings('.grid');
      if(href == undefined) {
        ins.loadDynamicProductsAjax($(this).attr('data-ajax-params'), 2, function(html) {
          ins.afterLoadDynamicProductsAjaxInTab(parent, html);
        });
      } else {
        ins.callAjax(href, 'GET', null, null, function(html) {
          ins.afterLoadDynamicProductsAjaxInTab(parent, html);
        });
      }
    });
  },

  updateThresholdMenu: function(addtionalHeight) {
    this.data.thresholdMenu += addtionalHeight;
  },

  calculateThresholdAndFixSticker: function() {
    var height = this.data.stickyMenuObj.outerHeight();
    this.data.thresholdMenu = this.data.stickyMenuObj.offset().top + height;
    this.fixedHeightSticker(height);
  },

  updateStickyMenuObj: function(obj) {
    if(obj.hasClass('use-sticky')) {
      this.data.stickyMenuObj = obj;
    } else {
      this.data.stickyMenuObj = obj.parents('.use-sticky').first();
    }
  },

  openStickyMenu: function() {
    if($(window).scrollTop() > this.data.thresholdMenu) {
      if(!this.data.stickyMenuObj.hasClass('active')) {
        this.data.stickyMenuObj.addClass('active');
      }
    } else {
      this.hideStickyMenu();
    }
  },

  hideStickyMenu: function() {
    if(this.data.stickyMenuObj.hasClass('active')) {
      this.data.stickyMenuObj.removeClass('active');
    } 
  },

  fixedHeightSticker: function(height) {
    this.data.sticker.height(height);
  },

  initStickyMenu: function() {
    if(!$('.use-sticky').length) {
      this.initPromotion();
      return;
    }
    var ins = this;
    var obj = $('.use-sticky--desktop');
    if(ins.data.cacheWindowWidth < 992) {
      obj = $('.use-sticky--mobile');
    }
    this.data.sticker = $('.header-sticker');
    this.updateStickyMenuObj(obj);
    this.calculateThresholdAndFixSticker();
    this.initPromotion();
    
    var timeout = 0;
    var lastScrollTop = 0; 
    $(window).on(this.data.scrollEventName, function(e, direction) {
      if(ins.data.pageLoaded) {
        if(direction == 'down') {
          ins.hideStickyMenu();
        } else {
          ins.openStickyMenu();
        }
      }
    });
    $(window).on(this.data.resizeEventName,function(e) {
      if(ins.data.cacheWindowWidth < 992) {
        obj = $('.use-sticky--mobile').removeClass('active');
      } else {
        obj = $('.use-sticky--desktop').removeClass('active');
        obj = $('.use-sticky--desktop');
        obj.parents('.use-sticky').removeClass('active');
      }
      ins.updateStickyMenuObj(obj);
      ins.calculateThresholdAndFixSticker();
    });
  },

  initStickyCartProductPage: function() {
    if($('.sc').length > 0) {
      var ins = this;
      $('.sc').removeClass('hide');
      $(window).on(this.data.scrollEventName, function(e, direction) {
        if(ins.data.pageLoaded) {
          if(direction == 'down') {
            var btn = $('.button--single-cart-main');
            var headerHeight = $('.sticky-menu.active').length > 0 ? $('.sticky-menu').height() : 0;
            if($(window).scrollTop() + headerHeight > btn.offset().top) {
              $('.sc').addClass('sc--active');
            } else {
              $('.sc').removeClass('sc--active');
            }
          } else {
            $('.sc').removeClass('sc--active');
          }
        }
      });

      var scSliding = false, statusClass = 'sc__close--working';
      if(ins.data.cacheWindowWidth < 992) {
        $('.sc__close').removeClass(statusClass);
        $('.sc__inner').hide();
        $('.sc').removeClass('open');
      } else {
        $('.sc__inner').show();
        $('.sc').addClass('open');
      }

      $('.sc__close').on('click', function(e) {
        e.preventDefault();
        if(scSliding) {
          return;
        }
        scSliding = true;
        $('.sc__inner').toggle('slow', function() {
          $('.sc__close').toggleClass(statusClass);
          scSliding = false;
          $('.sc').toggleClass('open');
        });
      });

      $('.sc__trigger').on('click touchend', function(e) {
        e.preventDefault();
        if(scSliding) {
          return;
        }
        scSliding = true;
        var effect = $('.sc').hasClass('open') ? 'slideUp': 'slideDown';
        if($('.sc').hasClass('open')) {
          $('.sc__inner').slideUp(300, toogleScClass);
        } else {
          $('.sc__inner').slideDown(300, toogleScClass);
        }
      });
      function toogleScClass() {
        $('.sc__close').toggleClass(statusClass);
        scSliding = false;
        $('.sc').toggleClass('open');
      }
      // Sync the main quantity input and this one in sticky panel
      $('.qty-box__input--single').on('change', function() {
        var input = this;
        $('.qty-box__input--single').each(function(){
          if(!$(this).is(input)) {
            $(this).val($(input).val());
          }
        });
      });
    }
  },

  alignReviewMasonryForm: function(){
    var reviewForm = $('.product-review-masonry');
    if(reviewForm.length > 0) {
      reviewForm.removeAttr('style');
      var mediaMasonry = $('.product-media-masonry-align');
      var offset = mediaMasonry.offset().top + mediaMasonry.outerHeight() + 20;
      var delta = offset - reviewForm.offset().top;
      if(delta < 0 ) {
        reviewForm.css('margin-top', delta + 'px');
      }
    }
  },

  initAlignReviewForm: function() {
    var reviewForm = $('.product-review-masonry');
    if(reviewForm.length > 0) {
      this.alignReviewMasonryForm();
      var ins = this;
      $(window).on(this.data.resizeEventName, function(e) {
        setTimeout(function() {
          ins.alignReviewMasonryForm();
        }, 300);
      });
    }
  },

  openRpInterval: function(minuteFrom, minuteTo, openTime, closeTime) {
    var selector = this.selectors.rp, ins = this;
    if($(selector.wrap).hasClass('hide')) {
      return;
    }
    var rp = $(selector.wrap);
    var length = $(selector.item).length - 1;
    var arrayIndex = Array.from(Array(length + 1).keys());
    if(arrayIndex.length == 0) {
      arrayIndex = Array.from(Array(length + 1).keys());
    }
    index = this.getRandomInt(0, arrayIndex.length - 1); 
    arrayIndex.splice(index, 1);
    $(selector.item).hide();
    var activeItem = $(selector.item + '[data-index="' + index +'"]');
    activeItem.find(selector.minuteNumber).html(this.getRandomInt(minuteFrom, minuteTo));
    activeItem.show();
    var image = activeItem.find('img'), delay = 0;
    if(!image.hasClass('loaded')) {
      delay = 300;
      image.attr('src', image.attr('data-src'));
      image.addClass('loaded');
    }
    setTimeout(function() {
      rp.slideDown(300, function() {
        ins.data.rp.openTimeInterval = setTimeout(function() {
          ins.hideRpInterval(minuteFrom, minuteTo, openTime, closeTime);
        }, openTime);
      });
    }, delay);
  },

  hideRpInterval: function(minuteFrom, minuteTo, openTime, closeTime) {
    var ins = this;
    $(this.selectors.rp.wrap).slideUp(ins.options.rp.duration, function() {
      ins.data.rp.openTimeInterval = setTimeout(function() {
        ins.openRpInterval(minuteFrom, minuteTo, openTime, closeTime);
      }, closeTime);
    });
  },

  initRecommendedProducts: function() {
    if(!theme.enableRecommendedProducts) {
      return;
    }
    
    setTimeout(function() {
      var cookieName = this.options.rp.cookieName;
      var cookie = this.getCookie(cookieName);
      if(cookie != undefined) {
        return;
      }
      var ins = this;
      ins.loadCssJsLib('recommendedProducts', function() {
        ins.loadSnippetAjax('recommended-products', function() {
          var selector = ins.selectors.rp;
          if($(selector.wrap).length > 0) {
            var rp = $(selector.wrap);
            var limit = rp.data('limit'),
            minuteFrom = rp.data('minute-from'),
            minuteTo = rp.data('minute-to'),
            openTime = rp.data('interval') * 1000,
            closeTime = rp.data('closing-interval') * 1000,
            index;
            var length = $(selector.item).length - 1;
            var list = [], i = 0, show = 0;
            for(i = 0; i < limit; i++){
              list.push(i);
            }
            list = ins.shuffleArray(list);
            if(cookie != 1) {
              setTimeout(function() {
                ins.openRpInterval(minuteFrom, minuteTo, openTime, closeTime);
              }, closeTime);
            }

            // Close recommended products
            $(document).on('click', selector.closeBtn, function(e) {
              e.preventDefault();
              clearTimeout(ins.data.rp.openTimeInterval);
              ins.setCookie(cookieName, 1, 1);
              $(selector.wrap).slideUp(ins.options.rp.duration);
            });
          }
        });
      });
    }.bind(this), 8000);
  },

  initNewsTerms: function(wrap) {
    if(!theme.newsTerms) {
      return;
    }
    var form = $(wrap + ' ' + this.selectors.newsTerms.form);
    var input = $(wrap + ' ' + this.selectors.newsTerms.input);
    var error = $(wrap + ' ' + this.selectors.newsTerms.error);
    var cookieName = this.options.newsTerms.cookieName;
    var cookieValue = this.getCookie(cookieName);
    var ins = this;
    
    form.unbind('submit');
    input.unbind('change');
    if(cookieValue == '1') {
      input.attr('checked','checked');
    }

    input.on('change', function() {
      if($(this).is(':checked')) {
        ins.setCookie(cookieName, 1);
        if(error.hasClass('working')) {
          error.fadeOut();
          error.removeClass('working');
        }
      } else {
        ins.setCookie(cookieName, 0);
      }
    });

    form.on('submit', function(e) {
      if(!input.is(':checked')) {
        e.preventDefault();
        error.fadeIn();
        error.addClass('working');
      }
    })
  },

  initNewsletterPopup: function() {
    if(!theme.enableNewsPopup || location.hash.substr(1) == 'newsletter-popup-form') {
      return;
    }
    var selector = this.selectors.newsPopup, ins = this;
    var cookieName = this.options.newsPopup.cookieName;
    setTimeout(function() {
      if($('body').hasClass('modal-open') || $('body').hasClass('open-search-suggest')) {
        return;
      }
      var cookie = this.getCookie(cookieName);
      new Promise(function(resolve, reject) {
        if(cookie == undefined || cookie == null || cookie == 'null') {
          ins.loadCssJsLib('newsletterPopup', function() {
            if($(selector.id).length > 0) {
              resolve();
            } else {
              ins.loadSnippetAjax('popup-newsletter', function() {
                resolve();
              }, 800);
            }
          });
        }
      }).then(function() {
        if($(selector.id).length > 0) {
          $(selector.id).removeClass('hide');
          ins.showPopup(selector.id);
          ins.initNewsTerms(selector.id);
        }
      }, function(err) {
        // console.log(err);
      });
    }.bind(this), 8000);
    $(document).on('hidden.bs.modal', selector.id, function () {
      if($(selector.checkbox).is(":checked")) {
        ins.setCookie(cookieName, 1, 1);
      }
    }); 
  },

  initPromotion: function() {
    var selector = this.selectors.promo, ins = this;
    if($(selector.wrap).length > 0) {
      var cookieName = this.options.promo.cookieName;
      var cookie = this.getCookie(cookieName);
      var hasStickyMenu = ($('.use-sticky').length > 0);
      if(cookie == undefined || cookie == null || cookie == 'null') {
        $(selector.wrap).slideDown(300, function() {
          if(hasStickyMenu) {
            if(ins.data.cacheWindowWidth >= 992) {
              ins.updateThresholdMenu($(selector.wrap).outerHeight());
            } else {
              ins.updateThresholdMenu(ins.data.thresholdMenuWithPromotion);
            }
          }
        });
      }
      $(document).on('click touchend', selector.closeBtn, function(e) {
        e.preventDefault();
        $(selector.wrap).slideUp(300, function() {
          if(hasStickyMenu) {
            if(ins.data.cacheWindowWidth >= 992) {
              ins.updateThresholdMenu($(selector.wrap).outerHeight()*(-1));
            } else {
              ins.updateThresholdMenu(ins.data.thresholdMenuWithPromotion*(-1));
            }
          }
        });
        ins.setCookie(cookieName, 1, 1);
      });
    }
  },

  initBackTopButton: function() {
    var $backTop = $('#back-top');
    if($backTop.length){
      $(document).on('click touchend', '#back-top a', function() {
        $('body,html').animate({
          scrollTop: 0
        }, 800);
        return false;
      });
    }
  },

  initSizeChart: function() {
    if($('.sizechart-trigger').length > 0) {
      var ins = this;
      $('.sizechart-trigger').unbind('click');
      $('.sizechart-trigger').on('click', function(e) {
        e.preventDefault();
        if($(this).hasClass('sizechart-trigger--inline')) {
          var sizechartTable = $('.sizechart-table');
          var sizechartTab = sizechartTable.parents('.tab-pane');
          var delay = 0;
          if(!sizechartTab.hasClass('.active')) {
            var tabId = sizechartTab.attr('id');
            $('.nav-tab-item a[href="#' + tabId + '"]').trigger('click');
            delay = 300;
          }
          setTimeout(function() {
            var offset = sizechartTable.offset().top - 80;
            if($('.use-sticky').length > 0) {
              offset -= 60;
            }
            offset = offset + 'px';
            $('body,html').animate({
              scrollTop: offset
            }, 800);
          }, delay);
        } else {
          var modalId = $(this).attr('data-modal-id');
          ins.showPopup('#' + modalId);
        }
      });
    }
    if($('.sizeChartModal .rte').length > 0) {
      $('.sizeChartModal').removeClass('hide');
      this.rteWrap('.sizeChartModal .rte');
    }
  },

  initCookieConsent: function() {
    if(!theme.enableCookieConsent) {
      return;
    }
    var ins = this;
    setTimeout(function() {
      var cookieName = 'cookie_consent';
      var cookie = ins.getCookie(cookieName);
      if(cookie != undefined) {
        return;
      }
      ins.loadSnippetAjax('cookie-consent', function() {
        if($('.cookie_consent').length > 0) {
          $('.cookie_consent').slideDown(300);
          $(document).on('click', '.cookie_consent__close', function(e) {
            e.preventDefault();
            ins.setCookie(cookieName, 1);
            $('.cookie_consent').slideUp(300);
          });
        }
      });
    }, 9000);
  },

  initPopupVideo: function() {
    var ins = this, selector = this.selectors.popupVideo, videoSrc;
    $(document).on('click', selector.btn, function(e) {
      e.preventDefault();
      if(ins.data.modalVideoWorking) {
        return;
      }
      var btn = $(this);
      new Promise(function(resolve, reject) {
        ins.data.modalVideoWorking = true;
        if(theme.loadedLibs.indexOf('modalvideo') > -1) {
          resolve(btn);
        } else {
          ins.loadCssJsLib('modalvideo', function() {
            $(selector.youtubeBtn).modalVideo();
            $(selector.vimeoBtn).modalVideo({channel:'vimeo'});
            resolve(btn);
          });
        }
      }).then(function(btn) {
        ins.data.modalVideoWorking = false;
        var videoId = btn.attr('data-video-id');
        var videoType = btn.attr('data-video-type');
        if(videoType == 'youtube') {
          $(selector.youtubeBtn).attr('data-video-id', videoId);
          $(selector.youtubeBtn).trigger('click');
        } else {
          $(selector.vimeoBtn).attr('data-video-id', videoId);
          $(selector.vimeoBtn).trigger('click');
        }
      }, function(err) {
        // console.log(err);
      });
    });
  },

  initTabAccordion: function() {
    var ins = this;
    $(document).on('click', '.tab-accordion__trigger', function(e) {
      e.preventDefault();
      if($(this).hasClass('working')) {
        return;
      }
      $(this).addClass('working');
      var btn = $(this);
      var tabContent = btn.parent().first();
      var tabList = tabContent.siblings('.nav-tabs');
      var newTabPane = $(btn.attr('href'));
      btn.toggleClass('open');
      
      if(btn.hasClass('open')) {
        newTabPane.slideDown(300, function() {
          btn.removeClass('working');
        });
      } else {
        newTabPane.slideUp(300, function() {
          btn.removeClass('working');
        });
      }
    });
    $(document).on('shown.bs.tab', '.tab-accordion-list a[data-toggle="tab"]', function (e) {
      var href = $(e.target).attr('href');
      var tabPane = $(href);
      tabPane.addClass('open').show();
      tabPane.siblings('.tab-accordion__trigger[href="' + href + '"]').addClass('open');
    });
    $(document).on('hidden.bs.tab', '.tab-accordion-list a[data-toggle="tab"]', function (e) {
      var href = $(e.target).attr('href');
      var tabPane = $(href);
      tabPane.removeClass('open').hide();
      tabPane.siblings('.tab-accordion__trigger[href="' + href + '"]').removeClass('open');
    });
  },

  loadWishlist: function(cb) { 
    var ins = this, selector = this.selectors.wishlist, cookieName = this.options.wishlist.cookieName;
    var cookieValue = this.getCookieItemsValue(false, cookieName);
    if(cookieValue == '' || cookieValue == undefined) {
      this.loadSnippetAjax('popup-wishlist', function() {
        var popupContent = $(selector.popup).find('.content');
        ins.appendWishlistEmtptyStr(popupContent);
        ins.data.wishlist.loaded = true;
        if(cb) {
          cb();
        }
      });
    } else {
      this.callAjax(theme.searchUrl, 'GET', {view: 'wishlist', q: cookieValue}, null, function(html) {
        ins.loadSnippetAjax('popup-wishlist', function() {
          var popupContent = $(selector.popup).find('.content');
          popupContent.html(ins.stripScripts(html));
          if(!popupContent.hasClass('grid')) {
            popupContent.addClass('grid grid--medium')
          }
          ins.applyCustomColorSwatches(popupContent);
          ins.convertCurrencySilence(popupContent.find('span.money'));
          ins.reLoadReview(popupContent);
          ins.data.wishlist.loaded = true;
          if(cb) {
            cb();
          }
        });
      });
    }
  },

  popularAddedWishlistItems: function(wrap){
    if(!theme.enableWishlist){
      return;
    }
    if(typeof wrap == 'string') {
      wrap = $(wrap);
    }
    var selector = this.selectors.wishlist, cookieName = this.options.wishlist.cookieName, ins = this;
    var btnSelector = '';
    var items = this.getCookieItemsValue(true, cookieName);
    if(items.length > 0) {
     $.each(items, function( index, value ) {
        if(btnSelector != '') {
          btnSelector += ',';
        }
        btnSelector += selector.btn + '[data-handle="' + value + '"]';
      });
    }
    wrap.find(btnSelector).addClass('added');
  },

  appendWishlistEmtptyStr: function(popupContent) {
    popupContent.removeClass('grid grid--medium')
    popupContent.html('<div class="alert alert-danger"><span>' + theme.strings.wishlistEmpty +'</span></div>');
  },

  initWishlist: function() {
    if(!theme.enableWishlist){
      return;
    }
    var selector = this.selectors.wishlist, cookieName = this.options.wishlist.cookieName, ins = this;
    var items = this.getCookieItemsValue(true, cookieName);
    var workingClass = 'working';
    this.popularAddedWishlistItems('body');
    $('.wishlist-number').text(items.length);
    $(document).on('click', selector.btn, function(e){
      e.preventDefault();
      var handle = $(this).attr('data-handle'), btn = $(this);
      if(ins.isProductExistInCookie(handle, cookieName)) {
        ins.removeProductFromCookie(handle, cookieName);
        $(selector.btn + '[data-handle="' + handle + '"]').removeClass('added');
      } else {
        ins.addProductToCookie(handle, cookieName);
        $(selector.btn + '[data-handle="' + handle + '"]').addClass('added');
      }
      $('.wishlist-number').text(ins.getCookieItemsValue(true, cookieName).length);
      ins.data.wishlist.loaded = false;
    });

    $(document).on('click', selector.removeBtn, function(e) {
      e.preventDefault();
      var handle = $(this).attr('data-handle');
      ins.removeProductFromCookie(handle, cookieName);
      $('.wishlist-number').text(ins.getCookieItemsValue(true, cookieName).length);
      $(selector.btn + '[data-handle="' + handle + '"]').removeClass('added');
      $(selector.popup + ' .item[data-product-handle="' + handle + '"]').remove();
      if($(selector.popup).find('.item').length == 0) {
        var popupContent = $(selector.popup).find('.content');
        ins.appendWishlistEmtptyStr(popupContent);
      }
    });

    $(document).on('click', selector.headerLink, function(e) {
      e.preventDefault();
      var btn = $(this);
      if(btn.hasClass(workingClass)) {
        return;
      }
      btn.addClass(workingClass);
      if(ins.data.wishlist.loaded) {
        ins.showPopup(selector.popup);
        btn.removeClass(workingClass);
      } else {
        ins.showLoadingFull();
        ins.loadWishlist(function() {
          ins.hideLoadingFull();
          ins.showPopup(selector.popup);
          btn.removeClass(workingClass);
        });  
      }
    });
  },

  initExpandTrigger: function() {
    var workingClass = 'working';
    $(document).on('click touchend', '.expand-trigger', function(e) {
      e.preventDefault();
      if($(this).hasClass('working')) {
        return;
      }
      var btn = $(this);
      btn.addClass('working');
      var effect = "slideDown";
      if(btn.hasClass('open')) {
        effect = "slideUp";
      }
      btn.toggleClass('open');
      if(effect == 'slideDown') {
        btn.siblings('.expand-content').slideDown(300, function() {
          btn.removeClass(workingClass);
        });
      } else {
        btn.siblings('.expand-content').slideUp(300, function() {
          btn.removeClass(workingClass);
        });
      }
    });
  },

  initResizeWindowTrigger: function() {
    this.data.cacheWindowWidth = $(window).width();
    var ins = this;
    $(window).resize($.debounce( 250, function(e) {
      var wwidth = $(window).width();
      if(ins.data.cacheWindowWidth!==wwidth){
        ins.data.cacheWindowWidth = $(window).width();
        ins.data.isTouchDevide = window.matchMedia("(any-pointer: coarse)").matches;
        $(window).trigger(ins.data.resizeEventName);
      }
    }));
  },

  initScrollingWindow: function() {
    var lastScrollTop = 0, ins = this;
    $(window).scroll($.throttle(100, function(e) {
      var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
      if (st > lastScrollTop){
        $(window).trigger(ins.data.scrollEventName, ['down']);
      } else {
        $(window).trigger(ins.data.scrollEventName, ['up']);
      }
      lastScrollTop = st <= 0 ? 0 : st;
    }));
    $(window).on(this.data.resizeEventName,function(e) {
      var st = window.pageYOffset || document.documentElement.scrollTop;
      lastScrollTop = st <= 0 ? 0 : st;  
    });
  },

  initScrollingWindowTriggerOnce: function(ele, suffixEvent, additionalOffset, cb) {
    additionalOffset = (additionalOffset != undefined) ? additionalOffset : 0;
    var offset = ele.offset().top + additionalOffset;
    if(window.pageYOffset + window.innerHeight > offset) {
      cb();
    } else {
      var ins = this, runCb = false, event = 'scroll.' + suffixEvent, resizeEvent = this.data.resizeEventName + '.scroll_' + suffixEvent;
      $(window).on(event, $.throttle( 250, function(e) {
        if(window.pageYOffset + window.innerHeight > offset) {
          $(window).off(event);
          $(window).off(resizeEvent);
          if(!runCb) {
            runCb = true;
            cb();
          }
        }
      }));
      $(window).on(resizeEvent , function() {
        if(runCb) {
          $(window).off(resizeEvent);
          return;
        }
        setTimeout(function() {
          offset = ele.offset().top + additionalOffset;
          if(window.pageYOffset + window.innerHeight > offset) {
            $(window).off(event);
            $(window).off(resizeEvent);
            if(!runCb) {
              runCb = true;
              cb();
            }
          }
        }, 300);
      });
    }
  },

  destroyScrollingWindowTriggerOnce: function(suffixEvent) {
    $(window).off('scroll.' + suffixEvent);
    $(window).off(this.data.resizeEventName + '.scroll_' + suffixEvent);
  },

  loadProductColumnsAjax: function($container) {
    var workingClass = 'working', ins = this;
    $container.find('.waiting-scroll-data').each(function() {
      var trigger = $(this);
      var wp = trigger.waypoint(function(direction) {
        if(trigger.hasClass(workingClass)) {
          return;
        }
        wp[0].destroy();
        trigger.addClass(workingClass);
        var parent = $(this.element).parent();
        ins.callAjax(theme.searchUrl, 'GET', {view: 'product_columns_ajax', q: trigger.attr('data-ajax-params')}, null, function(html) {
          trigger.remove();
          parent.append($('.products', html).html());
          ins.convertCurrencySilence(parent.find('span.money'));
          parent.removeClass('waiting-data');
          ins.initSlider(parent.parent(), false, false);
          ins.reLoadReview(parent);
        });
      }, {
        triggerOnce: true,
        offset: '100%'
      });
    });
  },

  addProductRecentView: function(productHandle) {
    if(!theme.enableRecentView) {
      return;
    }
    this.addProductToCookie(productHandle, this.options.recentView.cookieName);
  },

  enableModal: function() {
    $('.modal').removeClass('hide');
  },

  loadRequireCss: function() {
    if(theme.requireCss.length > 0) {
      var ins = this;
      $(theme.requireCss).each(function(index, value) {
        ins.loadCssJsLib(value);
      });
    }
  },

  init: function() {
    this.initFloatField();
    this.initMobileNavigation();
    this.initDropdown();
    this.initProductGroup();
    this.initMediaButton();
    this.initMainLightboxGallery();
    this.initCartEvent();
    this.initQtyBox();
    this.refreshSliderInTabs();
    this.initQuickView();
    this.initSelectOption();
    this.initCompareEvent();
    this.initWishlist();
    this.initLoadMoreBtn();
    this.initStickyMenu();
    this.initRecommendedProducts();
    this.initNewsletterPopup();
    this.initCookieConsent();
    this.initSearchSuggestion();
    this.initTabAccordion();
    this.initResizeWindowTrigger();
    this.initScrollingWindow();
    this.initExpandTrigger();
    $('body').tooltip({selector: '[data-toggle="tooltip"]', trigger: 'hover', container: 'body'}); 
    setTimeout(function() {
      this.updateFaviconBadge(theme.cartNumber);
      this.initBackTopButton();
      this.initAgreeCartTems();
      this.initPopupVideo();
    }.bind(this), 500);
    if(theme.disableCopy) {
      document.addEventListener('contextmenu', function(event){ event.preventDefault(); }); 
    }
  }
};

$(window).load(function() {
  BT.enableModal();
  BT.detectAutoFields();
  BT.initSidebar();
  BT.initAlignReviewForm();
  setTimeout(function() {
    BT.data.pageLoaded = true;
  }, 500);
  /*setTimeout(function() {
    window.lazySizesConfig.expand = 370;
    window.lazySizes.init();
  }, 5000);*/
});